package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddServiceInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

import retrofit2.http.Field;

public class AddServiceInteractorImpl extends AbstractInteractor implements AddServiceInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String authorization;
    int clientId;
    ArrayList<Integer> mainCategoryIds;
    ArrayList<Integer> subCategoryIds;
    ArrayList<Integer> lastCategoryIds;
    ArrayList<String> mrp;
    ArrayList<String> price;

    public AddServiceInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String authorization, int clientId, ArrayList<Integer> mainCategoryIds, ArrayList<Integer> subCategoryIds, ArrayList<Integer> lastCategoryIds, ArrayList<String> mrp, ArrayList<String> price) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.authorization = authorization;
        this.clientId = clientId;
        this.mainCategoryIds = mainCategoryIds;
        this.subCategoryIds = subCategoryIds;
        this.lastCategoryIds = lastCategoryIds;
        this.mrp = mrp;
        this.price = price;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onServiceAddFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onServiceAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.addService(authorization, clientId, mainCategoryIds, subCategoryIds, lastCategoryIds, mrp, price);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
