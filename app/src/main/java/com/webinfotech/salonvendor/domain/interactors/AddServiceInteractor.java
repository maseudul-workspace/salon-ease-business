package com.webinfotech.salonvendor.domain.interactors;

public interface AddServiceInteractor {
    interface Callback {
        void onServiceAddSuccess();
        void onServiceAddFail(String errorMsg, int loginError);
    }
}
