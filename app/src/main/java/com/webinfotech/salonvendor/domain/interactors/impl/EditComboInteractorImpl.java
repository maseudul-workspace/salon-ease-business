package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.EditComboInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepository;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class EditComboInteractorImpl extends AbstractInteractor implements EditComboInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String comboName;
    int mainCategoryId;
    ArrayList<String> serviceNames;
    ArrayList<String> mrps;
    ArrayList<String> prices;
    int comboId;
    ArrayList<Integer> serviceIds;

    public EditComboInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String comboName, int mainCategoryId, ArrayList<String> serviceNames, ArrayList<String> mrps, ArrayList<String> prices, int comboId, ArrayList<Integer> serviceIds) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.comboName = comboName;
        this.mainCategoryId = mainCategoryId;
        this.serviceNames = serviceNames;
        this.mrps = mrps;
        this.prices = prices;
        this.comboId = comboId;
        this.serviceIds = serviceIds;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onComboEditFail(loginError, errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onComboEditSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.editCombo(authorization, comboName, mainCategoryId, serviceNames, mrps, prices, comboId, serviceIds);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
