package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfile {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("work_experience")
    @Expose
    public String workExperience;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("gst")
    @Expose
    public String gst;

    @SerializedName("latitude")
    @Expose
    public double latitude;

    @SerializedName("longitude")
    @Expose
    public double longitude;

    @SerializedName("opening_time")
    @Expose
    public String opening_time;

    @SerializedName("closing_time")
    @Expose
    public String closing_time;

    @SerializedName("address_proof")
    @Expose
    public String addressProof;

    @SerializedName("address_proof_file")
    @Expose
    public String addressProofFile;

    @SerializedName("photo_proof")
    @Expose
    public String photoProof;

    @SerializedName("photo_proof_file")
    @Expose
    public String photoProofFile;

    @SerializedName("business_proof")
    @Expose
    public String businessProof;

    @SerializedName("business_proof_file")
    @Expose
    public String businessProofFile;

    @SerializedName("client_type")
    @Expose
    public int client_type;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("profile_status")
    @Expose
    public int profile_status;

    @SerializedName("verify_status")
    @Expose
    public int verify_status;

    @SerializedName("ac")
    @Expose
    public int ac;

    @SerializedName("parking")
    @Expose
    public int parking;

    @SerializedName("wifi")
    @Expose
    public int wifi;

    @SerializedName("music")
    @Expose
    public int music;

    @SerializedName("api_token")
    @Expose
    public String api_token;

    @SerializedName("service_city_id")
    @Expose
    public int service_city_id;

    @SerializedName("job_status")
    @Expose
    public int job_status;

    @SerializedName("image_upload_left_count")
    @Expose
    public int image_upload_left_count;

    @SerializedName("services")
    @Expose
    public Service[] services;

    @SerializedName("images")
    @Expose
    public Images[] images;

    @SerializedName("client_schedules")
    @Expose
    public Schedule[] schedules;

}
