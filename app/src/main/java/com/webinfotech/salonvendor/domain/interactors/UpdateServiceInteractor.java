package com.webinfotech.salonvendor.domain.interactors;

public interface UpdateServiceInteractor {
    interface Callback {
        void onUpdateServiceSuccess();
        void onUpdateServiceFailed(String errorMsg, int loginError);
    }
}
