package com.webinfotech.salonvendor.domain.interactors;

public interface EditComboInteractor {
    interface Callback {
        void onComboEditSuccess();
        void onComboEditFail(int loginError, String errorMsg);
    }
}
