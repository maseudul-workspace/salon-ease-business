package com.webinfotech.salonvendor.domain.models;

public class AddedCombo {

    public String comboName;
    public String mrp;
    public String price;

    public AddedCombo(String comboName, String mrp, String price) {
        this.comboName = comboName;
        this.mrp = mrp;
        this.price = price;
    }
}
