package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.SendOtpForgetPasswordInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.OtpResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class SendOtpForgetPasswordInteractorImpl extends AbstractInteractor implements SendOtpForgetPasswordInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public SendOtpForgetPasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOtpSendFail(errorMsg);
            }
        });
    }

    private void postMessage(String otp){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOtpSendSuccess(otp);
            }
        });
    }

    @Override
    public void run() {
        OtpResponse otpResponse = mRepository.sendOtp(mobile);
        if (otpResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!otpResponse.status) {
            notifyError(otpResponse.message);
        } else {
            postMessage(otpResponse.otp);
        }
    }
}
