package com.webinfotech.salonvendor.domain.interactors;

public interface SendOtpInteractor {
    interface Callback {
        void onOtpSendSuccess(String otp);
        void onOtpSendFail(String errorMsg);
    }
}
