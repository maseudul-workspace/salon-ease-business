package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Subcategory;

public interface FetchSubcategoryInteractor {
    interface Callback {
        void onGettingSubcategorySuccess(Subcategory[] subcategories);
        void ongGettingSubcategoryFail(String errorMsg);
    }
}
