package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;

public interface FetchThirdCategoryInteractor {
    interface Callback {
        void onGettingThirdCategorySuccess(ThirdCategory[] thirdCategories);
        void ongGettingThirdCategoryFail(String errorMsg);
    }
}
