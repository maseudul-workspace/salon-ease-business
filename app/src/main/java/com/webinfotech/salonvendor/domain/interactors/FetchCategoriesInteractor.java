package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Category;

public interface FetchCategoriesInteractor {
    interface Callback {
        void onGettingCategoriesSuccess(Category[] categories);
        void onGettingCategoriesFail();
    }
}
