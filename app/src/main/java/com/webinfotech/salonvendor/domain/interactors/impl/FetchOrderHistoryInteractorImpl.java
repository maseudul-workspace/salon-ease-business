package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.Orders;
import com.webinfotech.salonvendor.domain.models.OrdersWrapper;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;


public class FetchOrderHistoryInteractorImpl extends AbstractInteractor implements FetchOrderHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Orders[] orders){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orders);
            }
        });
    }

    @Override
    public void run() {
        final OrdersWrapper ordersWrapper = mRepository.fetchOrderHistory(apiToken, userId);
        if (ordersWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!ordersWrapper.status) {
            notifyError(ordersWrapper.message, ordersWrapper.login_error);
        } else {
            postMessage(ordersWrapper.orders);
        }
    }
}
