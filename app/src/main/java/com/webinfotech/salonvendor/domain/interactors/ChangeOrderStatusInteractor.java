package com.webinfotech.salonvendor.domain.interactors;

public interface ChangeOrderStatusInteractor {
    interface Callback {
        void onOrderStatusChangedSuccess();
        void onOrderStatusChangedFail(String errorMsg, int loginError);
    }
}
