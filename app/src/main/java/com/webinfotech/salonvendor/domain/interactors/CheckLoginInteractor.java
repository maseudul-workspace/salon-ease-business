package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.UserDetails;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserDetails userDetails);
        void onLoginFail(String errorMsg);
    }
}
