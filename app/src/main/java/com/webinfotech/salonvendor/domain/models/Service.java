package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Service {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("main_category_id")
    @Expose
    public int mainCategoryId;

    @SerializedName("main_category")
    @Expose
    public String mainCategory;

    @SerializedName("main_category_image")
    @Expose
    public String mainCategoryImage;

    @SerializedName("sub_category_id")
    @Expose
    public int subCategoryId;

    @SerializedName("sub_category")
    @Expose
    public String subCategory;

    @SerializedName("sub_category_image")
    @Expose
    public String subCategoryImage;

    @SerializedName("last_category_id")
    @Expose
    public int lastCategoryId;

    @SerializedName("last_category")
    @Expose
    public String lastCategory;

    @SerializedName("last_category_image")
    @Expose
    public String lastCategoryImage;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("expire_date")
    @Expose
    public String expiryDate;

    @SerializedName("is_deal")
    @Expose
    public String isDeal;

    @SerializedName("discount")
    @Expose
    public double discount;

}
