package com.webinfotech.salonvendor.domain.models;

public class AddedService {

    public int mainCategoryId;
    public String mainCategoryName;
    public String mainCategoryImage;
    public int subCategoryId;
    public String subCategoryName;
    public String subCategoryImage;
    public int thirdCategoryId;
    public String thirdCategoryName;
    public String thirdCategoryImage;
    public String mrp;
    public String price;

    public AddedService(int mainCategoryId, String mainCategoryName, String mainCategoryImage, int subCategoryId, String subCategoryName, String subCategoryImage, int thirdCategoryId, String thirdCategoryName, String thirdCategoryImage, String mrp, String price) {
        this.mainCategoryId = mainCategoryId;
        this.mainCategoryName = mainCategoryName;
        this.mainCategoryImage = mainCategoryImage;
        this.subCategoryId = subCategoryId;
        this.subCategoryName = subCategoryName;
        this.subCategoryImage = subCategoryImage;
        this.thirdCategoryId = thirdCategoryId;
        this.thirdCategoryName = thirdCategoryName;
        this.thirdCategoryImage = thirdCategoryImage;
        this.mrp = mrp;
        this.price = price;
    }
}
