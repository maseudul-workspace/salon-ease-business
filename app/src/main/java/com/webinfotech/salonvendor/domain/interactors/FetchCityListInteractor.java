package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.City;

public interface FetchCityListInteractor {
    interface Callback {
        void onGettingCityListSuccess(City[] cities);
        void onGettingCityListFail(String errorMsg);
    }
}
