package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceOrder {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("service_id")
    @Expose
    public int serviceId;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("sub_category_name")
    @Expose
    public String subCategoryName;

    @SerializedName("third_category_name")
    @Expose
    public String thirdCategoryName;

    @SerializedName("amount")
    @Expose
    public String amount;

}
