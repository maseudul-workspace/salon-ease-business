package com.webinfotech.salonvendor.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.DeleteGalleryImageInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class DeleteGalleryImageInteractorImpl extends AbstractInteractor implements DeleteGalleryImageInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int userId;
    int imageId;

    public DeleteGalleryImageInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String apiToken, int userId, int imageId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.imageId = imageId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onImageDeleteFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onImageDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.deleteGalleryImage(apiToken, userId, imageId);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
