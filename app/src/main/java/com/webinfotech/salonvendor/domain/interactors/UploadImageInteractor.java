package com.webinfotech.salonvendor.domain.interactors;

public interface UploadImageInteractor {
    interface Callback {
        void onFileUploadSuccess();
        void onFileUploadFail(String errorMsg);
    }
}
