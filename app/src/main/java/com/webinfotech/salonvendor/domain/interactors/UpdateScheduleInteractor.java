package com.webinfotech.salonvendor.domain.interactors;

public interface UpdateScheduleInteractor {
    interface Callback {
        void onScheduleUpdateSuccess();
        void onScheduleUpdateFail(String errorMsg, int loginError);
    }
}
