package com.webinfotech.salonvendor.domain.interactors;

public interface UpdateFirebaseInteractor {
    interface Callback {
        void onFirebaseTokenUpdateSuccess();
        void onFirebaseTokenUpdateFail();
    }
}
