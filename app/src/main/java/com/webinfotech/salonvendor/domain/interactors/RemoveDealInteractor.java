package com.webinfotech.salonvendor.domain.interactors;

public interface RemoveDealInteractor {
    interface Callback {
        void onDealRemoveSuccess();
        void onDealRemoveFail(String errorMsg, int loginError);
    }
}
