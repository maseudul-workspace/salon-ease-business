package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orders {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("advance_amount")
    @Expose
    public double advanceAmount;

    @SerializedName("payment_request_id")
    @Expose
    public String paymentRequestId;

    @SerializedName("payment_status")
    @Expose
    public int paymentStatus;

    @SerializedName("order-status")
    @Expose
    public int orderStatus;

    @SerializedName("service_time")
    @Expose
    public String serviceTime;

    @SerializedName("refund_request")
    @Expose
    public int refundRequest;

    @SerializedName("vendor_details")
    @Expose
    public OrderVendorDetails vendorDetails;

    @SerializedName("service_data")
    @Expose
    public ServiceOrder[] serviceOrders;

    @SerializedName("customer_address")
    @Expose
    public UserProfile userProfile;

}
