package com.webinfotech.salonvendor.domain.interactors;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterUserSuccess();
        void onRegisterUserFail(String errorMsg);
    }
}
