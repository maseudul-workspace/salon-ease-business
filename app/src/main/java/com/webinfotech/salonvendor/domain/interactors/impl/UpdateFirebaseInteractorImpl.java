package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class UpdateFirebaseInteractorImpl extends AbstractInteractor implements UpdateFirebaseInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String token;

    public UpdateFirebaseInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCalback, AppRepositoryImpl mRepository, String apiToken, int userId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCalback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.token = token;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateFirebaseToken(apiToken, userId, token);
        if (commonResponse == null) {
            notifyError();
        } else if (!commonResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
