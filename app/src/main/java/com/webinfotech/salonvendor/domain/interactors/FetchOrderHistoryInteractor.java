package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Orders;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Orders[] orders);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
