package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.domain.models.ComboServiceData;

public interface FetchCombosInteractor {
    interface Callback {
        void onGettingCombosSuccess(ComboServiceData[] comboServiceData);
        void onGettingCombosFail(String errorMsg, int loginError);
    }
}
