package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("man")
    @Expose
    public int man;

    @SerializedName("woman")
    @Expose
    public int woman;

    @SerializedName("kids")
    @Expose
    public int kids;

}
