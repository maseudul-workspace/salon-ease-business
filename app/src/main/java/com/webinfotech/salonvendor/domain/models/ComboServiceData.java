package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComboServiceData {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category_id")
    @Expose
    public int categoryId;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("combo_name")
    @Expose
    public String comboName;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("combo_services")
    @Expose
    public ComboService[] comboServices;

}
