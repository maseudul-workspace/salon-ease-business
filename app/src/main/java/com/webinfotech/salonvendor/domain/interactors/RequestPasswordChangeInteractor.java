package com.webinfotech.salonvendor.domain.interactors;

public interface RequestPasswordChangeInteractor {
    interface Callback {
        void onRequestPasswordChangedSuccess();
        void onRequestPasswordChangedFail(String errorMsg);
    }
}
