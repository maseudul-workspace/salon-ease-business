package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchCombosInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.ComboDataWrapper;
import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.domain.models.ComboServiceData;
import com.webinfotech.salonvendor.domain.models.Orders;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class FetchCombosInteractorImpl extends AbstractInteractor implements FetchCombosInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;

    public FetchCombosInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCombosFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ComboServiceData[] comboServicesData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCombosSuccess(comboServicesData);
            }
        });
    }

    @Override
    public void run() {
        final ComboDataWrapper comboDataWrapper = mRepository.fetchCombos(apiToken);
        if (comboDataWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!comboDataWrapper.status) {
            notifyError(comboDataWrapper.message, comboDataWrapper.login_error);
        } else {
            postMessage(comboDataWrapper.comboServiceData);
        }
    }
}
