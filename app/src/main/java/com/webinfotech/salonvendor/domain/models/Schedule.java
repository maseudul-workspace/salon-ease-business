package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("status")
    @Expose
    public int status;

}
