package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.UpdateServiceInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class UpdateServiceInteractorImpl extends AbstractInteractor implements UpdateServiceInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int serviceId;
    String authorization;
    String mrp;
    String price;
    int mainCategoryId;
    int subCategoryId;
    int lastCategoryId;

    public UpdateServiceInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int serviceId, String mrp,
                                       String price,
                                       int mainCategoryId,
                                       int subCategoryId,
                                       int lastCategoryId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.serviceId = serviceId;
        this.mrp = mrp;
        this.price = price;
        this.mainCategoryId = mainCategoryId;
        this.subCategoryId = subCategoryId;
        this.lastCategoryId = lastCategoryId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateServiceFailed(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateServiceSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateService(authorization, serviceId, mrp, price, mainCategoryId, subCategoryId, lastCategoryId);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
