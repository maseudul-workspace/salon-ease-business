package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class UpdateProfileInteractorImpl extends AbstractInteractor implements UpdateProfileInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String authorization;
    int clientId;
    String name;
    String mobile;
    String workExperience;
    String state;
    String city;
    int serviceCityId;
    String address;
    String pin;
    String email;
    String gst;
    double latitude;
    double longitude;
    String openingTime;
    String closing_time;
    String description;
    String profileImageFilePath;
    boolean isNewImage;
    String addressProofFile;
    String addressProof;
    String photoProofFile;
    String photoProof;
    String businessProofFile;
    String businessProof;
    int ac;
    int parking;
    int wifi;
    int music;
    boolean isNewIdProof;
    boolean isNewBusinessProof;
    boolean isNewAddressProof;

    public UpdateProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String authorization, int clientId, String name, String mobile, String workExperience, String state, String city, int serviceCityId, String address, String pin, String email, String gst, double latitude, double longitude, String openingTime, String closing_time, String description, String profileImageFilePath, boolean isNewImage, String addressProofFile, String addressProof, String photoProofFile, String photoProof, String businessProofFile, String businessProof, int ac, int parking, int wifi, int music, boolean isNewIdProof, boolean isNewBusinessProof, boolean isNewAddressProof) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.authorization = authorization;
        this.clientId = clientId;
        this.name = name;
        this.mobile = mobile;
        this.workExperience = workExperience;
        this.state = state;
        this.city = city;
        this.serviceCityId = serviceCityId;
        this.address = address;
        this.pin = pin;
        this.email = email;
        this.gst = gst;
        this.latitude = latitude;
        this.longitude = longitude;
        this.openingTime = openingTime;
        this.closing_time = closing_time;
        this.description = description;
        this.profileImageFilePath = profileImageFilePath;
        this.isNewImage = isNewImage;
        this.addressProofFile = addressProofFile;
        this.addressProof = addressProof;
        this.photoProofFile = photoProofFile;
        this.photoProof = photoProof;
        this.businessProofFile = businessProofFile;
        this.businessProof = businessProof;
        this.ac = ac;
        this.parking = parking;
        this.wifi = wifi;
        this.music = music;
        this.isNewAddressProof = isNewAddressProof;
        this.isNewBusinessProof = isNewBusinessProof;
        this.isNewIdProof = isNewIdProof;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateProfile( authorization,
                                                                                clientId,
                                                                                name,
                                                                                mobile,
                                                                                workExperience,
                                                                                state,
                                                                                city,
                                                                                serviceCityId,
                                                                                address,
                                                                                pin,
                                                                                email,
                                                                                gst,
                                                                                latitude,
                                                                                longitude,
                                                                                openingTime,
                                                                                closing_time,
                                                                                description,
                                                                                profileImageFilePath,
                                                                                isNewImage,
                                                                                addressProofFile,
                                                                                addressProof,
                                                                                photoProofFile,
                                                                                photoProof,
                                                                                businessProofFile,
                                                                                businessProof,
                                                                                ac,
                                                                                parking,
                                                                                wifi,
                                                                                music,
                                                                                isNewIdProof,
                                                                                isNewBusinessProof,
                                                                                isNewAddressProof);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
