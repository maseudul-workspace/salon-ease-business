package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdCategory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("top_category_id")
    @Expose
    public int topCategoryId;

    @SerializedName("sub_category_id")
    @Expose
    public int subCategoryId;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("third_level_category_name")
    @Expose
    public String name;

}
