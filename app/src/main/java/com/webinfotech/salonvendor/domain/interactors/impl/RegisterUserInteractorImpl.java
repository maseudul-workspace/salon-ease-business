package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.RegisterUserInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.domain.models.RegistrationResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import retrofit2.http.Field;

public class RegisterUserInteractorImpl extends AbstractInteractor implements RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String mobile;
    String password;
    String otp;
    int clientType;
    int serviceCityId;
    double latitude;
    double longitude;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String mobile, String password, String otp, int clientType, int serviceCityId, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.mobile = mobile;
        this.password = password;
        this.otp = otp;
        this.clientType = clientType;
        this.serviceCityId = serviceCityId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterUserFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterUserSuccess();
            }
        });
    }

    @Override
    public void run() {
        final RegistrationResponse commonResponse = mRepository.registerUser(name, mobile, password, otp, clientType, serviceCityId, latitude, longitude);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            if (commonResponse.error) {
                if (commonResponse.errorMesage.email != null) {
                    notifyError(commonResponse.errorMesage.email[0]);
                } else if (commonResponse.errorMesage.mobile != null) {
                    notifyError(commonResponse.errorMesage.mobile[0]);
                } else {
                    notifyError(commonResponse.message);
                }
            } else {
                notifyError(commonResponse.message);
            }
        } else {
            postMessage();
        }
    }
}
