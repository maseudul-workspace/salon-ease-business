package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.UserProfile;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserProfile userProfile);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
