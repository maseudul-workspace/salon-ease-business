package com.webinfotech.salonvendor.domain.interactors;

public interface DeleteGalleryImageInteractor {
    interface Callback {
        void onImageDeleteSuccess();
        void onImageDeleteFail(String errorMsg, int loginError);
    }
}
