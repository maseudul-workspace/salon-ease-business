package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddDealInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class AddDealInteractorImpl extends AbstractInteractor implements AddDealInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int clientServiceId;
    String discount;
    String expiryDate;

    public AddDealInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int clientServiceId, String discount, String expiryDate) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.clientServiceId = clientServiceId;
        this.discount = discount;
        this.expiryDate = expiryDate;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDealAddFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDealAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.addDeal(apiToken, clientServiceId, discount, expiryDate);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
