package com.webinfotech.salonvendor.domain.interactors;

public interface ResetPasswordInteractor {
    interface Callback {
        void onPasswordResetSuccess();
        void onPasswordResetFail(String errorMsg);
    }
}
