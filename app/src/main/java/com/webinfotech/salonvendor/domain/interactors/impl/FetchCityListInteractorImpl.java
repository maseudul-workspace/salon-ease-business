package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.CityWrapper;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class FetchCityListInteractorImpl extends AbstractInteractor implements FetchCityListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchCityListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCityListFail(errorMsg);
            }
        });
    }

    private void postMessage(City[] cities){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCityListSuccess(cities);
            }
        });
    }

    @Override
    public void run() {
        final CityWrapper cityWrapper = mRepository.fetchCityList();
        if (cityWrapper == null) {
            notifyError("");
        } else if (!cityWrapper.status) {
            notifyError("");
        } else {
            postMessage(cityWrapper.cities);
        }
    }
}
