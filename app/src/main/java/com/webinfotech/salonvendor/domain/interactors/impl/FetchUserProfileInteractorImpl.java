package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.domain.models.UserProfileWrapper;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserProfile userProfile){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserProfileSuccess(userProfile);
            }
        });
    }

    @Override
    public void run() {
        final UserProfileWrapper userProfileWrapper = mRepository.fetchUserProfile(apiToken, userId);
        if (userProfileWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!userProfileWrapper.status) {
            notifyError(userProfileWrapper.message, userProfileWrapper.login_error);
        } else {
            postMessage(userProfileWrapper.userProfile);
        }
    }

}
