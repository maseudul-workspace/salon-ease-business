package com.webinfotech.salonvendor.domain.interactors;

public interface AddDealInteractor {
    interface Callback {
        void onDealAddSuccess();
        void onDealAddFail(String errorMsg, int loginError);
    }
}
