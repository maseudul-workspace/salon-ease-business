package com.webinfotech.salonvendor.domain.interactors;

public interface SendOtpForgetPasswordInteractor {
    interface Callback {
        void onOtpSendSuccess(String otp);
        void onOtpSendFail(String errorMsg);
    }
}
