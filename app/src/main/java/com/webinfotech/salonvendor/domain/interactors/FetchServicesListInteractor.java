package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Service;

public interface FetchServicesListInteractor {
    interface Callback {
        void onGettingServicesListSuccess(Service[] services);
        void onGettingServicesListFail(String errorMsg, int loginError);
    }
}
