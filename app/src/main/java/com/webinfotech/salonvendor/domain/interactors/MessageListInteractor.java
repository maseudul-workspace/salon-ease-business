package com.webinfotech.salonvendor.domain.interactors;


import com.webinfotech.salonvendor.domain.models.MessageData;

public interface MessageListInteractor {
    interface Callback {
        void onMessageListFetchSuccess(MessageData[] messageData, int totalPage);
        void onMessageListFetchFail(String errorMsg, int loginError);
    }
}
