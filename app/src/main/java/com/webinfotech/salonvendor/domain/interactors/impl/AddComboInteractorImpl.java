package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddComboInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class AddComboInteractorImpl extends AbstractInteractor implements AddComboInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String comboName;
    int mainCategoryId;
    ArrayList<String> serviceNames;
    ArrayList<String> mrp;
    ArrayList<String> price;

    public AddComboInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String comboName, int mainCategoryId, ArrayList<String> serviceNames, ArrayList<String> mrp, ArrayList<String> price) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.comboName = comboName;
        this.mainCategoryId = mainCategoryId;
        this.serviceNames = serviceNames;
        this.mrp = mrp;
        this.price = price;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onComboAddFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onComboAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.addCombo(authorization, comboName, mainCategoryId, serviceNames, mrp, price);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
