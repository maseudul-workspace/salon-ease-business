package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.CategoryWrapper;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class FetchCategoriesInteractorImpl extends AbstractInteractor implements FetchCategoriesInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public FetchCategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoriesFail();
            }
        });
    }

    private void postMessage(Category[] categories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoriesSuccess(categories);
            }
        });
    }

    @Override
    public void run() {
        final CategoryWrapper categoryWrapper = mRepository.fetchCategories();
        if (categoryWrapper == null) {
            notifyError();
        } else if (!categoryWrapper.status) {
            notifyError();
        } else {
            postMessage(categoryWrapper.categories);
        }
    }
}
