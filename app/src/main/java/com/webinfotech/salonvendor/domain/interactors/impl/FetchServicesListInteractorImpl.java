package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchServicesListInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.domain.models.ServicesWrapper;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.repository.AppRepository;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class FetchServicesListInteractorImpl extends AbstractInteractor implements FetchServicesListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int clientId;

    public FetchServicesListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int clientId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.clientId = clientId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingServicesListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Service[] services){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingServicesListSuccess(services);
            }
        });
    }

    @Override
    public void run() {
        final ServicesWrapper servicesWrapper = mRepository.fetchServiceList(apiToken, clientId);
        if (servicesWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!servicesWrapper.status) {
            notifyError(servicesWrapper.message, servicesWrapper.login_error);
        } else {
            postMessage(servicesWrapper.services);
        }
    }

}
