package com.webinfotech.salonvendor.domain.interactors;

public interface AddComboInteractor {
    interface Callback {
        void onComboAddSuccess();
        void onComboAddFail(String errorMsg, int loginError);
    }
}
