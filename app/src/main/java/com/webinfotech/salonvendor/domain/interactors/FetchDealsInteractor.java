package com.webinfotech.salonvendor.domain.interactors;

import com.webinfotech.salonvendor.domain.models.Service;

public interface FetchDealsInteractor {
    interface Callback {
        void onGettingDealsListSuccess(Service[] services);
        void onGettingDealsListFail(String errorMsg, int loginError);
    }
}
