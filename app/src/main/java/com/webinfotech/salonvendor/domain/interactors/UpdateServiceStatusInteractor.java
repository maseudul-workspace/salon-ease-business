package com.webinfotech.salonvendor.domain.interactors;

public interface UpdateServiceStatusInteractor {
    interface Callback {
        void onServiceUpdateSuccess();
        void onServiceUpdateFail(String errorMsg, int loginError);
    }
}
