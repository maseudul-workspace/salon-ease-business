package com.webinfotech.salonvendor.domain.interactors.impl;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.CheckLoginInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserDetailsWrapper;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String phone;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String phone, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.phone = phone;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserDetails userDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userDetails);
            }
        });
    }

    @Override
    public void run() {
        final UserDetailsWrapper userDetailsWrapper = mRepository.checkLogin(phone, password);
        if (userDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!userDetailsWrapper.status) {
            notifyError(userDetailsWrapper.message);
        } else {
            postMessage(userDetailsWrapper.userDetails);
        }
    }
}
