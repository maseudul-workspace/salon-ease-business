package com.webinfotech.salonvendor.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Images {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("client_id")
    @Expose
    public int clientId;

    @SerializedName("image")
    @Expose
    public String image;

}
