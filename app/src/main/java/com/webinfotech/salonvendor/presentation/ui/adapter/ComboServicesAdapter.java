package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.ComboService;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComboServicesAdapter extends RecyclerView.Adapter<ComboServicesAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int comboServiceId, int serviceId);
    }

    Context mContext;
    ComboService[] comboServices;
    Callback mCallback;

    public ComboServicesAdapter(Context mContext, ComboService[] comboServices, Callback mCallback) {
        this.mContext = mContext;
        this.comboServices = comboServices;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_combo_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboName.setText((position + 1) + ". " + comboServices[position].name);
        holder.txtViewMrp.setText("Rs " + comboServices[position].mrp);
        holder.txtViewPrice.setText("Rs " + comboServices[position].price);

        holder.layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(comboServices[position].jobId, comboServices[position].id);
            }
        });
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return comboServices.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_combo_name)
        TextView txtViewComboName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.layout_edit)
        View layoutEdit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
