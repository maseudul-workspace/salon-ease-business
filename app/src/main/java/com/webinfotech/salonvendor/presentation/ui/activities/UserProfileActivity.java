package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Schedule;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.UserProfilePresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.ImageGalleryAdapter;
import com.webinfotech.salonvendor.presentation.ui.adapter.ServiceAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.EditPriceDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;
import com.webinfotech.salonvendor.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.webinfotech.salonvendor.util.Helper.calculateFileSize;
import static com.webinfotech.salonvendor.util.Helper.getDateAhead;
import static com.webinfotech.salonvendor.util.Helper.getDateAheadFormat;
import static com.webinfotech.salonvendor.util.Helper.getRealPathFromURI;
import static com.webinfotech.salonvendor.util.Helper.saveImage;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View, ImageGalleryAdapter.Callback, ServiceAdapter.Callback, EditPriceDialog.Callback {

    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    @BindView(R.id.img_view_preview)
    ImageView imgViewPreview;
    @BindView(R.id.view_add_image)
    View viewAddImage;
    @BindView(R.id.layout_selected_image)
    View layoutSelectedImage;
    String filePath;
    UserProfilePresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.recycler_view_image_gallery)
    RecyclerView recyclerViewImageGallery;
    @BindView(R.id.recycler_view_services)
    RecyclerView recyclerViewServices;
    @BindView(R.id.txt_view_salon_name)
    TextView txtViewSalonName;
    @BindView(R.id.img_view_salon)
    ImageView imgViewSalon;
    @BindView(R.id.switch_first_date)
    SwitchMaterial switchFirstDate;
    @BindView(R.id.switch_second_date)
    SwitchMaterial switchSecondDate;
    @BindView(R.id.switch_third_date)
    SwitchMaterial switchThirdDate;
    @BindView(R.id.image_container)
    View imgContainer;
    @BindView(R.id.btn_complete_profile)
    Button btnCompleteProfile;
    int position;
    ImageGalleryAdapter galleryAdapter;
    UserProfile userProfile;
    boolean firstLoad = true;
    Category[] categories;
    EditPriceDialog editPriceDialog;
    int editMainCategoryId = 0;
    int editSubCategoryId = 0;
    int editLastCategoryId = 0;
    int editServiceId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Profile");
        setUpProgressDialog();
        setEditPriceDialog();
        initialisePresenter();
        setSwitchText();
    }

    private void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setSwitchText() {
        switchFirstDate.setText(getDateAhead(1));
        switchSecondDate.setText(getDateAhead(2));
        switchThirdDate.setText(getDateAhead(3));
        switchFirstDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPresenter.updateSchedule(getDateAheadFormat(1), 2);
                } else {
                    mPresenter.updateSchedule(getDateAheadFormat(1), 1);
                }
            }
        });

        switchSecondDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPresenter.updateSchedule(getDateAheadFormat(2), 2);
                } else {
                    mPresenter.updateSchedule(getDateAheadFormat(2), 1);
                }
            }
        });

        switchThirdDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mPresenter.updateSchedule(getDateAheadFormat(3), 2);
                } else {
                    mPresenter.updateSchedule(getDateAheadFormat(3), 1);
                }
            }
        });

    }

    private void setEditPriceDialog() {
        editPriceDialog = new EditPriceDialog(this, this, this);
        editPriceDialog.setUpDialogView();;
    }

    private boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0){
                loadImageChooser();
            }

            else{
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    }
                    else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIC) {
            if (data.getData() != null) {
                GlideHelper.setImageViewWithURI(this, imgViewPreview, data.getData());
                filePath = getRealPathFromURI(data.getData(), this);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    viewAddImage.setVisibility(View.GONE);
                    layoutSelectedImage.setVisibility(View.VISIBLE);
                }
            } else {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imgViewPreview.setImageBitmap(photo);
                filePath = saveImage(photo);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    viewAddImage.setVisibility(View.GONE);
                    layoutSelectedImage.setVisibility(View.VISIBLE);
                }

            }
        }
    }


    @OnClick(R.id.view_add_image) void onAddImageClicked() {
       if (checkAndRequestPermissions()) {
           if (userProfile != null && userProfile.images.length == 10) {
               Toast.makeText(this, "Maximum 10 images are allowed", Toast.LENGTH_LONG).show();
           } else {
               loadImageChooser();
           }
       }
    }

    private void loadImageChooser() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        layoutSelectedImage.setVisibility(View.GONE);
        viewAddImage.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_save) void onSaveBtnClicked() {
        mPresenter.submitFile(filePath);
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public void loadUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        if (userProfile.images.length == 0 || userProfile.images == null) {
            imgContainer.setVisibility(View.GONE);
        } else {
            galleryAdapter = new ImageGalleryAdapter(this, userProfile.images, this);
            recyclerViewImageGallery.setAdapter(galleryAdapter);
            recyclerViewImageGallery.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            imgContainer.setVisibility(View.VISIBLE);
        }

        ServiceAdapter serviceAdapter = new ServiceAdapter(this, userProfile.services, this);
        recyclerViewServices.setAdapter(serviceAdapter);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(this));

        txtViewSalonName.setText(userProfile.name);

        GlideHelper.setImageViewCustomRoundedCorners(this, imgViewSalon, getResources().getString(R.string.base_url) + "images/client/" + userProfile.image, 150);

        Schedule[] schedules = userProfile.schedules;
        for (int i = 0; i < schedules.length; i++) {
            if (schedules[i].date.equals(getDateAheadFormat(1)) && schedules[i].status == 1) {
                switchFirstDate.setChecked(false);
            }
            if (schedules[i].date.equals(getDateAheadFormat(2)) && schedules[i].status == 1) {
                switchSecondDate.setChecked(false);
            }
            if (schedules[i].date.equals(getDateAheadFormat(3)) && schedules[i].status == 1) {
                switchThirdDate.setChecked(false);
            }
        }

        if (userProfile.profile_status == 1) {
            btnCompleteProfile.setText("Complete Profile");
        } else {
            btnCompleteProfile.setText("Edit Profile");
        }

    }

    @Override
    public void onFileUploadSuccess() {
        onCancelClicked();
    }

    @Override
    public void onImageDeleteSuccess() {
        galleryAdapter.notifyItemRemoved(position);
    }

    @Override
    public void loadCategories(Category[] categories) {
        this.categories = categories;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onImageClicked(int id) {

    }

    @Override
    public void onDeleteClicked(int id, int position) {
        mPresenter.deleteImage(id);
        this.position = position;
    }

    @Override
    public void onEditClicked(Service service) {
        editPriceDialog.setPrice(service.mrp, Double.toString(service.price));
        editPriceDialog.showDialog();
        editMainCategoryId = service.mainCategoryId;
        editSubCategoryId = service.subCategoryId;
        editLastCategoryId = service.lastCategoryId;
        editServiceId = service.id;
    }

    @Override
    public void updateServiceStatus(int serviceId, int status) {
        mPresenter.updateServiceStatus(serviceId, status);
    }

    @OnClick(R.id.btn_complete_profile) void onCompleteProfileClicked() {
        firstLoad = true;
        Intent intent = new Intent(this, ProfileCompleteActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_add_service) void onAddService() {
       Intent intent = new Intent(this, AddServiceActivity.class);
       startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchUserProfile();
        mPresenter.fetchCategories();
        firstLoad = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void editPrice(String mrp, String price) {
        mPresenter.updateService(editServiceId, mrp, price, editMainCategoryId, editSubCategoryId, editLastCategoryId);
    }
}