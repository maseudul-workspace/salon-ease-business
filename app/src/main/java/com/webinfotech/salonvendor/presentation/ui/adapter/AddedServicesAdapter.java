package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.AddedService;
import com.webinfotech.salonvendor.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddedServicesAdapter extends RecyclerView.Adapter<AddedServicesAdapter.ViewHolder> {

    Context mContext;
    ArrayList<AddedService> addedServices;

    public AddedServicesAdapter(Context mContext, ArrayList<AddedService> addedServices) {
        this.mContext = mContext;
        this.addedServices = addedServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_added_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewMainCategory.setText(addedServices.get(position).mainCategoryName + " -> " + addedServices.get(position).subCategoryName);
        holder.txtViewSubcategory.setText(addedServices.get(position).thirdCategoryName);
        holder.txtViewMrp.setText("Rs. " + addedServices.get(position).mrp);
        holder.txtViewPrice.setText("Rs. " + addedServices.get(position).price);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewMainCategoryIcon, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + addedServices.get(position).thirdCategoryImage, 10);
    }

    @Override
    public int getItemCount() {
        return addedServices.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category)
        ImageView imgViewMainCategoryIcon;
        @BindView(R.id.txt_view_main_category)
        TextView txtViewMainCategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
