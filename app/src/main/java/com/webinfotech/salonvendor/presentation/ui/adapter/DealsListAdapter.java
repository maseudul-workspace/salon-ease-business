package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DealsListAdapter extends RecyclerView.Adapter<DealsListAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int position);
        void onRemoveClicked(int serviceId);
    }

    Context mContext;
    Service[] services;
    Callback mCallback;

    public DealsListAdapter(Context mContext, Service[] services, Callback mCallback) {
        this.mContext = mContext;
        this.services = services;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_deals, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewMainCategory.setText(services[position].mainCategory + " -> " + services[position].subCategory);
        holder.txtViewSubcategory.setText(services[position].lastCategory);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewMainCategoryIcon, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + services[position].lastCategoryImage, 100);
        holder.txtViewDiscount.setText(services[position].discount + "%");
        holder.txtViewExpiryDate.setText("Expiry Date: " + services[position].expiryDate);
        holder.txtViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(position);
            }
        });
        holder.txtViewRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onRemoveClicked(services[position].id);
            }
        });
        holder.txtViewMrp.setText("Rs. " + services[position].mrp);
        double price = services[position].price - services[position].price * services[position].discount / 100;
        holder.txtViewPrice.setText("Rs. " + price);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category)
        ImageView imgViewMainCategoryIcon;
        @BindView(R.id.txt_view_main_category)
        TextView txtViewMainCategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.txt_view_edit)
        TextView txtViewEdit;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;
        @BindView(R.id.txt_view_expiry_date)
        TextView txtViewExpiryDate;
        @BindView(R.id.txt_view_remove)
        TextView txtViewRemove;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
