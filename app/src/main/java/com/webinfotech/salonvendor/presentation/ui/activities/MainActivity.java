package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.MainPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.CancelReasonDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;
import com.webinfotech.salonvendor.util.GlideHelper;

public class MainActivity extends AppCompatActivity implements MainPresenter.View {

    ProgressDialog progressDialog;
    MainPresenterImpl mPresenter;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.layout_complete_profile)
    View layoutCompleteProfile;
    @BindView(R.id.img_view_user)
    ImageView imgViewUser;
    @BindView(R.id.txt_view_user_name)
    TextView txtViewUserName;
    int UPDATE_REQUEST_CODE = 1200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        ButterKnife.bind(this);
        setBottomNavigationView();
        getLoginInfo();
        setUpProgressDialog();
        initialsiePresenter();
        checkUpdate();
        mPresenter.updateFirebaseToken(refreshedToken);
    }

    private void initialsiePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_profile:
                        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_about:
                        Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(aboutIntent);
                        break;
                    case R.id.nav_notification:
                        Intent notificationIntent = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(notificationIntent);
                        break;
                    case R.id.nav_contact_us:
                        Intent contactusIntent = new Intent(getApplicationContext(), ContactusActivity.class);
                        startActivity(contactusIntent);
                        break;
                }
                return false;
            }
        });
    }

    private void getLoginInfo() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(this);
        if (userDetails != null) {
//            Log.e("LogMsg", "Api Token: " + userDetails.api_token);
//            Log.e("LogMsg", "User Id: " + userDetails.id);
        }
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void checkUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });

        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
            }
        });

    }

    @Override
    public void loadUserProfile(UserProfile userProfile) {
        if (userProfile.image != null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUser, getResources().getString(R.string.base_url) + "images/client/" + userProfile.image, 150);
        }
        txtViewUserName.setText("Welcome, " + userProfile.name);
        if (userProfile.profile_status == 2) {
            layoutCompleteProfile.setVisibility(View.GONE);
        } else {
            layoutCompleteProfile.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchUserProfile();
    }

    @OnClick(R.id.btn_complete_profile) void onCompleteProfileClicked() {
        Intent intent = new Intent(this, ProfileCompleteActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_booking_history) void onBookingHistoryClicked() {
        Intent intent = new Intent(this, BookingHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_my_deals) void onMyDealsClicked() {
        Intent intent = new Intent(this, MyDealsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_my_combos) void onMyCombosClicked() {
        Intent intent = new Intent(this, MyCombosActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_user_profile) void onUserProfileClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_add_service) void onAddServiceClicked() {
        Intent intent = new Intent(this, AddServiceActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}