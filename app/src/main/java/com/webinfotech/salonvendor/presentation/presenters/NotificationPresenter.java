package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.presentation.ui.adapter.NotificationListRecyclerViewAdapter;

public interface NotificationPresenter {
    void fetchMessageList(int page, String refresh);
    interface View {
        void loadAdapter(NotificationListRecyclerViewAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void onLoginError();
    }
}
