package com.webinfotech.salonvendor.presentation.presenters;

public interface ChangePasswordPresenter {
    void changePassword(String currentPassword, String newPassword);
    interface View {
        void showLoader();
        void hideLoader();
        void onPasswordChangedSuccess();
    }
}
