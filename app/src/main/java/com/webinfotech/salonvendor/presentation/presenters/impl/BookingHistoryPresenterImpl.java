package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.ChangeOrderStatusInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.ChangeOrderStatusInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.salonvendor.domain.models.Orders;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.BookingHistoryPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class BookingHistoryPresenterImpl extends AbstractPresenter implements   BookingHistoryPresenter, 
                                                                                FetchOrderHistoryInteractor.Callback, 
                                                                                OrdersAdapter.Callback,
                                                                                ChangeOrderStatusInteractor.Callback
{
    
    Context mContext;
    BookingHistoryPresenter.View mView;
    private int orderId;

    public BookingHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchBookingHistory() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchOrderHistoryInteractorImpl fetchOrderHistoryInteractor = new FetchOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.id, userDetails.api_token);
            fetchOrderHistoryInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void cancelOrder(String reason) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            ChangeOrderStatusInteractorImpl changeOrderStatusInteractor = new ChangeOrderStatusInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, orderId, 5, reason);
            changeOrderStatusInteractor.execute();
            mView.showLoader();
        } else {
        }
    }

    @Override
    public void onGettingOrderHistorySuccess(Orders[] orders) {
        OrdersAdapter ordersAdapter = new OrdersAdapter(mContext, orders, this);
        mView.loadAdapter(ordersAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.hideViews();
    }

    @Override
    public void changeOrderStatus(int orderId, int status) {
        this.orderId = orderId;
        if (status == 5) {
            mView.onCancelClicked();
        } else {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            UserDetails userDetails = androidApplication.getUserInfo(mContext);
            if (userDetails != null) {
                ChangeOrderStatusInteractorImpl changeOrderStatusInteractor = new ChangeOrderStatusInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, orderId, status, null);
                changeOrderStatusInteractor.execute();
                mView.showLoader();
            } else {

            }
        }
    }

    @Override
    public void onLocationClicked(double latitude, double longitude) {
        mView.goToMapLocation(latitude, longitude);
    }

    @Override
    public void onOrderStatusChangedSuccess() {
        mView.hideLoader();
        fetchBookingHistory();
    }

    @Override
    public void onOrderStatusChangedFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
