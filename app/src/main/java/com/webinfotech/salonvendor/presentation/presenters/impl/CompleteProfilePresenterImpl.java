package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.CompleteProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class CompleteProfilePresenterImpl extends AbstractPresenter implements CompleteProfilePresenter, FetchUserProfileInteractor.Callback, FetchCityListInteractor.Callback,
                                                                                UpdateProfileInteractor.Callback {

    Context mContext;
    CompleteProfilePresenter.View mView;

    public CompleteProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchUserProfileInteractorImpl fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        } else {

        }
    }

    @Override
    public void fetchCities() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
            fetchCityListInteractor.execute();
        } else {

        }
    }

    @Override
    public void updateProfile(String name, String mobile, String workExperience, String state, String city, int serviceCityId, String address, String pin, String email, String gst, double latitude, double longitude, String openingTime, String closing_time, String description, String profileImageFilePath, boolean isNewImage, String addressProofFile, String addressProof, String photoProofFile, String photoProof, String businessProofFile, String businessProof, int ac, int parking, int wifi, int music, boolean isNewIdProof, boolean isNewBusinessProof, boolean isNewAddressProof) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UpdateProfileInteractorImpl updateProfileInteractor = new UpdateProfileInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userDetails.api_token, userDetails.id,  name,
                    mobile,
                    workExperience,
                    state,
                    city,
                    serviceCityId,
                    address,
                    pin,
                    email,
                    gst,
                    latitude,
                    longitude,
                    openingTime,
                    closing_time,
                    description,
                    profileImageFilePath,
                    isNewImage,
                    addressProofFile,
                    addressProof,
                    photoProofFile,
                    photoProof,
                    businessProofFile,
                    businessProof,
                    ac,
                    parking,
                    wifi,
                    music,
                    isNewIdProof,
                    isNewBusinessProof,
                    isNewAddressProof
            );
            updateProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {

    }

    @Override
    public void onGettingUserProfileSuccess(UserProfile userProfile) {
        mView.loadUserProfile(userProfile);
        mView.hideLoader();
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onProfileUpdateSuccess() {
        Toast.makeText(mContext, "Profile Updated Successfully", Toast.LENGTH_LONG).show();
        mView.hideLoader();
        mView.onProfileUpdateSuccess();
    }

    @Override
    public void onProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

}
