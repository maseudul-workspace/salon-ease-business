package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.SendOtpInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.SendOtpInteractorImpl;
import com.webinfotech.salonvendor.presentation.presenters.RegistrationOtpVerifyPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class RegistrationOtpPresenterImpl extends AbstractPresenter implements  RegistrationOtpVerifyPresenter,
                                                                                SendOtpInteractor.Callback {

    Context mContext;
    RegistrationOtpVerifyPresenter.View mView;

    public RegistrationOtpPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendOtpInteractorImpl sendOtpInteractor = new SendOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, 2);
        sendOtpInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOtpSendSuccess(String otp) {
        mView.hideLoader();
        mView.onOtpSendSuccess(otp);
    }

    @Override
    public void onOtpSendFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
