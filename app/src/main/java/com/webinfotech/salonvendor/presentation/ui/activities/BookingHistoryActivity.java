package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.BookingHistoryPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.BookingHistoryPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.CancelReasonDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class BookingHistoryActivity extends AppCompatActivity implements BookingHistoryPresenter.View,  CancelReasonDialog.Callback  {

    @BindView(R.id.recycler_view_booking_history)
    RecyclerView recyclerViewBookingHistory;
    @BindView(R.id.layout_no_bookings)
    View layoutNoBookings;
    BookingHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    CancelReasonDialog cancelReasonDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Booking History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        setCancelReasonDialog();
        mPresenter.fetchBookingHistory();
    }

    private void initialisePresenter() {
        mPresenter = new BookingHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setCancelReasonDialog() {
        cancelReasonDialog = new CancelReasonDialog(this, this, this);
        cancelReasonDialog.setUpDialogView();
    }

    @Override
    public void loadAdapter(OrdersAdapter adapter) {
        recyclerViewBookingHistory.setVisibility(View.VISIBLE);
        layoutNoBookings.setVisibility(View.GONE);
        recyclerViewBookingHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewBookingHistory.setAdapter(adapter);
    }

    @Override
    public void goToMapLocation(double latitude, double longitude) {
        String geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + "()";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
        startActivity(intent);
    }

    @Override
    public void onCancelClicked() {
        cancelReasonDialog.showDialog();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void hideViews() {
        recyclerViewBookingHistory.setVisibility(View.GONE);
        layoutNoBookings.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCancelClicked(String reason) {
        cancelReasonDialog.hideDialog();
        mPresenter.cancelOrder(reason);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}