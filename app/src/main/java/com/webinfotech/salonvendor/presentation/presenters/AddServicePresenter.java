package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;

import java.util.ArrayList;

import retrofit2.http.Field;

public interface AddServicePresenter {
    void fetchCategories();
    void fetchSubcategories(int categoryId);
    void fetchThirdCategories(int categoryId);
    void addService(ArrayList<Integer> mainCategoryIds,
                    ArrayList<Integer> subCategoryIds,
                    ArrayList<Integer> lastCategoryIds,
                    ArrayList<String> mrp,
                    ArrayList<String> price);
    interface View {
        void loadCategories(Category[] categories);
        void loadSubcategories(Subcategory[] subcategories);
        void loadThirdCategories(ThirdCategory[] thirdCategories);
        void onServiceAddSuccess();
        void showLoader();
        void hideLoader();
    }
}
