package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.City;

public interface RegisterUserPresenter {
    void registerUser( String name,
                       String mobile,
                       String password,
                       String otp,
                       int clientType,
                       int serviceCityId,
                       double latitude,
                       double longitude
                      );
    void fetchCityList();
    interface View {
        void loadCities(City[] cities);
        void onUserRegistrationSuccess();
        void showLoader();
        void hideLoader();
    }
}
