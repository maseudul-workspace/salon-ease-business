package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.loader.content.CursorLoader;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.CompleteProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.CompleteProfilePresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.dialogs.SelectCityDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;
import com.webinfotech.salonvendor.util.GlideHelper;
import com.webinfotech.salonvendor.util.Helper;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.webinfotech.salonvendor.util.Helper.calculateFileSize;
import static com.webinfotech.salonvendor.util.Helper.getRealPathFromURI;
import static com.webinfotech.salonvendor.util.Helper.saveImage;

public class ProfileCompleteActivity extends AppCompatActivity implements CompleteProfilePresenter.View, OnMapReadyCallback,
                                                                            LocationListener, GoogleApiClient.ConnectionCallbacks,
                                                                            GoogleApiClient.OnConnectionFailedListener,
                                                                            SelectCityDialog.Callback{

    @BindView(R.id.layout_bottomsheet)
    NestedScrollView layoutBottomsheet;
    @BindView(R.id.txt_view_location_address)
    TextView txtViewLocationAddress;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_work_experience)
    EditText editTextWorkExperience;
    @BindView(R.id.edit_text_gst_no)
    EditText editTextGstNo;
    @BindView(R.id.edit_text_opening_time)
    EditText editTextOpeningTime;
    @BindView(R.id.edit_text_closing_time)
    EditText editTextClosingTime;
    @BindView(R.id.img_view_user_profile)
    ImageView imgViewUserProfile;
    @BindView(R.id.edit_text_description)
    EditText editTextDescription;
    @BindView(R.id.txt_input_gst_no)
    TextInputLayout txtInputGstNo;
    BottomSheetBehavior sheetBehavior;
    private GoogleMap mMap;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    double latitude;
    double longitude;
    UserProfile userProfile;
    String openingTime = "";
    String closingTime = "";
    SelectCityDialog selectCityDialog;
    City[] cities;
    int serviceCityId;
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    private static final int REQUEST_ID_PROOF = 1001;
    private static final int REQUEST_ADDRESS_PROOF = 1002;
    private static final int REQUEST_BUSINESS_PROOF = 1003;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 1200;
    String filePath = null;
    boolean isNewImage = false;
    ProgressDialog progressDialog;
    CompleteProfilePresenterImpl mPresenter;
    boolean firstLoad = true;
    String[] appPremisions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    @BindView(R.id.spinner_id_proof)
    Spinner spinnerIdProof;
    @BindView(R.id.spinner_address_proof)
    Spinner spinnerAddressProof;
    @BindView(R.id.spinner_business_address_proof)
    Spinner spinnerBusinessProof;
    @BindView(R.id.img_id_proof)
    ImageView imgViewIdProof;
    @BindView(R.id.img_address_proof)
    ImageView imgViewAddressProof;
    @BindView(R.id.img_business_proof)
    ImageView imgViewBusinessProof;
    String idProofPath = null;
    String addressProofPath = null;
    String businessProofPath = null;
    @BindView(R.id.layout_id_proof)
    View layoutIdProof;
    @BindView(R.id.layout_address_proof)
    View layoutAddressProof;
    @BindView(R.id.layout_business_proof)
    View layoutBusinessProof;
    @BindView(R.id.switch_air_condition)
    SwitchMaterial switchAirCondition;
    @BindView(R.id.switch_music)
    SwitchMaterial switchMusic;
    @BindView(R.id.switch_parking)
    SwitchMaterial switchParking;
    @BindView(R.id.switch_wifi)
    SwitchMaterial switchWifi;
    int ac = 1;
    int wifi = 1;
    int parking = 1;
    int music = 1;
    String idProofType = null;
    String addressProofType = null;
    String businessProofType = null;
    @BindView(R.id.txt_view_verification_status)
    TextView txtViewVerifyStatus;
    boolean isNewAddressProof = false;
    boolean isNewIdProof = false;
    boolean isNewBusinessProof = false;
    @BindView(R.id.layout_features)
    View layoutFeatures;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_complete);
        getSupportActionBar().setTitle("Edit Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            if (savedInstanceState.getString("profilePath") != null) {
                filePath = savedInstanceState.getString("profilePath");
                GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, filePath, 150);
            }
            if (savedInstanceState.getString("idProofPath") != null) {
                idProofPath = savedInstanceState.getString("idProofPath");
                GlideHelper.setImageView(this, imgViewIdProof, idProofPath);
            }
            if (savedInstanceState.getString("addressProofPath") != null) {
                addressProofPath = savedInstanceState.getString("addressProofPath");
                GlideHelper.setImageView(this, imgViewAddressProof, addressProofPath);
            }
            if (savedInstanceState.getString("businessProofPath") != null) {
                businessProofPath = savedInstanceState.getString("businessProofPath");
                GlideHelper.setImageView(this, imgViewBusinessProof, businessProofPath);
            }
            isNewImage = savedInstanceState.getBoolean("isNewImage", false);
            isNewBusinessProof = savedInstanceState.getBoolean("isNewBusinessProof", false);
            isNewAddressProof = savedInstanceState.getBoolean("isNewAddressProof", false);
            isNewIdProof = savedInstanceState.getBoolean("isNewIdProof", false);
        }
        setSwitchListeners();
        setSpinners();
        setLayoutBottomsheet();
        setUpCityDialog();
        setUpProgressDialog();
        initialisePresenter();
        if (checkAndRequestPermissions()) {
            if (firstLoad) {
                mPresenter.fetchUserProfile();
                mPresenter.fetchCities();
                firstLoad = false;
            }
        }

    }

    private void initialisePresenter() {
        mPresenter = new CompleteProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setSwitchListeners() {

        switchAirCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ac = 2;
                } else {
                    ac = 1;
                }
            }
        });

        switchMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    music = 2;
                } else {
                    music = 1;
                }
            }
        });

        switchParking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    parking = 2;
                } else {
                    parking = 1;
                }
            }
        });

        switchWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    wifi = 2;
                } else {
                    wifi = 1;
                }
            }
        });
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setUpCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this, this);
        selectCityDialog.setUpDialogView();
    }

    private void setLayoutBottomsheet() {
        sheetBehavior = BottomSheetBehavior.from(layoutBottomsheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        sheetBehavior.setPeekHeight(400);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void setSpinners() {

        final ArrayAdapter<String> idProofAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text_layout, getResources().getStringArray(R.array.id_proof)){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerIdProof.setAdapter(idProofAdapter);
        spinnerIdProof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    idProofType = getResources().getStringArray(R.array.id_proof)[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> addressProofAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text_layout, getResources().getStringArray(R.array.address_proof)){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerAddressProof.setAdapter(addressProofAdapter);
        spinnerAddressProof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    addressProofType = getResources().getStringArray(R.array.address_proof)[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter<String> businessAdapter = new ArrayAdapter<String>(this, R.layout.spinner_text_layout, getResources().getStringArray(R.array.business_proof)){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerBusinessProof.setAdapter(businessAdapter);
        spinnerBusinessProof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    businessProofType = getResources().getStringArray(R.array.business_proof)[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void loadUserProfile(UserProfile userProfile) {
        if (userProfile.client_type == 1) {
            isNewBusinessProof = true;
        }
        this.userProfile = userProfile;
        editTextName.setText(userProfile.name);
        editTextEmail.setText(userProfile.email);
        editTextGstNo.setText(userProfile.gst);
        editTextMobile.setText(userProfile.mobile);
        editTextWorkExperience.setText(userProfile.workExperience);
        editTextPin.setText(userProfile.pin);
        try {
            editTextOpeningTime.setText(changeDateFormat(userProfile.opening_time));
            editTextClosingTime.setText(changeDateFormat(userProfile.closing_time));
        } catch (NullPointerException e) {

        }
        editTextDescription.setText(userProfile.description);
        openingTime = userProfile.opening_time;
        closingTime = userProfile.closing_time;
        serviceCityId = userProfile.service_city_id;
        latitude = userProfile.latitude;
        longitude = userProfile.longitude;
        initialiseMap();
        if (userProfile.client_type == 1) {
            layoutBusinessProof.setVisibility(View.GONE);
            layoutFeatures.setVisibility(View.GONE);
            txtInputGstNo.setVisibility(View.GONE);
        }

        if (this.userProfile.verify_status == 1) {
            txtViewVerifyStatus.setText("Account verification pending");
            txtViewVerifyStatus.setTextColor(getResources().getColor(R.color.md_blue_500));
        } else if (this.userProfile.verify_status == 2) {
            txtViewVerifyStatus.setText("Account verified");
            txtViewVerifyStatus.setTextColor(getResources().getColor(R.color.md_green_500));
        } else {
            txtViewVerifyStatus.setText("Your verification is rejected");
            txtViewVerifyStatus.setTextColor(getResources().getColor(R.color.md_red_500));
        }

        if (filePath == null && userProfile.image != null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, getResources().getString(R.string.base_url) + "images/client/" + userProfile.image, 150);
            filePath = userProfile.image;
        } else if (filePath != null && filePath.equals(userProfile.image)) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, getResources().getString(R.string.base_url) + "images/client/" + userProfile.image, 150);
        }

        if (addressProofPath == null && userProfile.addressProofFile != null) {
            GlideHelper.setImageView(this, imgViewAddressProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.addressProofFile);
            addressProofPath = userProfile.addressProofFile;
        } else if (addressProofPath != null && addressProofPath.equals(userProfile.addressProofFile)) {
            GlideHelper.setImageView(this, imgViewAddressProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.addressProofFile);
        }

        if (idProofPath == null && userProfile.photoProofFile != null) {
            GlideHelper.setImageView(this, imgViewIdProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.photoProofFile);
            idProofPath = userProfile.photoProofFile;
        } else if (idProofPath != null && idProofPath.equals(userProfile.photoProofFile)) {
            GlideHelper.setImageView(this, imgViewIdProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.photoProofFile);
        }

        if (businessProofPath == null && userProfile.businessProofFile != null) {
            GlideHelper.setImageView(this, imgViewBusinessProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.businessProofFile);
            businessProofPath = userProfile.businessProofFile;
        } else if (businessProofPath != null && businessProofPath.equals(userProfile.businessProofFile)) {
            GlideHelper.setImageView(this, imgViewBusinessProof, getResources().getString(R.string.base_url) + "images/client/files/" + userProfile.businessProofFile);
        }

        if (userProfile.photoProof != null) {
            switch (userProfile.photoProof) {
                case "Driving License":
                    spinnerIdProof.setSelection(1);
                    break;
                case "Voter Id":
                    spinnerIdProof.setSelection(2);
                    break;
                case "PAN Card":
                    spinnerIdProof.setSelection(3);
                    break;
                case "Adhar Card":
                    spinnerIdProof.setSelection(4);
                    break;
                case "Passport":
                    spinnerIdProof.setSelection(5);
                    break;
            }
        }

        if (userProfile.addressProof != null) {
            switch (userProfile.photoProof) {
                case "Driving License":
                    spinnerAddressProof.setSelection(1);
                    break;
                case "Voter Id":
                    spinnerAddressProof.setSelection(2);
                    break;
                case "PAN Card":
                    spinnerAddressProof.setSelection(3);
                    break;
                case "Adhar Card":
                    spinnerAddressProof.setSelection(4);
                    break;
            }
        }

        if (userProfile.businessProof != null) {
            switch (userProfile.businessProof) {
                case "Trade License":
                    spinnerBusinessProof.setSelection(1);
                    break;
            }
        }

        if (userProfile.ac == 2) {
            switchAirCondition.setChecked(true);
        }

        ac = userProfile.ac;

        if (userProfile.parking == 2) {
            switchParking.setChecked(true);
        }

        parking = userProfile.parking;

        if (userProfile.music == 2) {
            switchMusic.setChecked(true);
        }

        music = userProfile.music;

        if (userProfile.wifi == 2) {
            switchWifi.setChecked(true);
        }

        wifi = userProfile.wifi;

    }

    @Override
    public void loadCities(City[] cities) {
        selectCityDialog.setRecyclerView(cities);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onProfileUpdateSuccess() {
        finish();
    }

    private String changeDateFormat(String startTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        Date dt;
        try {
            dt = sdf.parse(startTime);
            return sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String changeTimeFormat(String startTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat sdfs = new SimpleDateFormat("HH:mm:ss");
        Date dt;
        try {
            dt = sdf.parse(startTime);
            return sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
       getLocation();
    }

    private void getLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi
                        .checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied.
                        // You can initialize location requests here.
                        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ProfileCompleteActivity.this::onLocationChanged);
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied.
                        // But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            // Ask to turn on GPS automatically
                            startIntentSenderForResult(status.getResolution().getIntentSender(), REQUEST_CHECK_SETTINGS_GPS, null, 0, 0, 0, null);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied.
                        // However, we have no way
                        // to fix the
                        // settings so we won't show the dialog.
                        // finish();
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        setUpMapListener();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (latitude < 1 && longitude < 1 ) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    buildGoogleApiClient();
                    mMap.setMyLocationEnabled(true);
                }
            } else {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            setUpMapListener();
        }
    }

    private void setUpMapListener() {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.draggable(true);
        markerOptions.visible(false);
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng centerLatLang = mMap.getProjection().getVisibleRegion().latLngBounds.getCenter();
                latitude = centerLatLang.latitude;
                longitude = centerLatLang.longitude;
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(centerLatLang.latitude, centerLatLang.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String postalCode = addresses.get(0).getPostalCode();
                    if (city != null) {
                        editTextCity.setText(city);
                    }
                    if (state != null) {
                        editTextState.setText(state);
                    }
                    if (postalCode != null) {
                        editTextPin.setText(postalCode);
                    }
                    if (address != null) {
                        editTextAddress.setText(address);
                        txtViewLocationAddress.setText(address);
                    }
                } catch (Exception e) {

                }
            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void onCitySelected(int id, String cityName) {
        editTextCity.setText(cityName);
        selectCityDialog.hideDialog();
        serviceCityId = id;
    }

    @OnClick(R.id.edit_text_opening_time) void onOpeningTimeClick() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        editTextOpeningTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }

    @OnClick(R.id.edit_text_closing_time) void onClosingTimeClick() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        editTextClosingTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
        timePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        timePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
    }

    @OnClick(R.id.edit_text_city) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextName.getText().toString().trim().isEmpty()) {
            editTextName.getParent().requestChildFocus(editTextName, editTextName);
            editTextName.requestFocus();
        } else if (editTextMobile.getText().toString().trim().isEmpty()) {
            editTextMobile.getParent().requestChildFocus(editTextMobile, editTextMobile);
            editTextMobile.requestFocus();
        } else if (editTextMobile.getText().toString().trim().length() != 10) {
            editTextMobile.getParent().requestChildFocus(editTextMobile, editTextMobile);
            editTextMobile.requestFocus();
            Toast.makeText(this, "Phone No must be 10 digits", Toast.LENGTH_SHORT).show();
        } else if (editTextWorkExperience.getText().toString().trim().isEmpty()) {
            editTextWorkExperience.getParent().requestChildFocus(editTextWorkExperience, editTextWorkExperience);
            editTextWorkExperience.requestFocus();
        } else if (editTextCity.getText().toString().trim().isEmpty()) {
            editTextCity.getParent().requestChildFocus(editTextCity, editTextCity);
            editTextCity.requestFocus();
        } else if (editTextState.getText().toString().trim().isEmpty()) {
            editTextState.getParent().requestChildFocus(editTextState, editTextState);
            editTextState.requestFocus();
        } else if (editTextPin.getText().toString().trim().isEmpty()) {
            editTextPin.getParent().requestChildFocus(editTextPin, editTextPin);
            editTextPin.requestFocus();
        } else if ( editTextAddress.getText().toString().trim().isEmpty()) {
            editTextAddress.getParent().requestChildFocus(editTextAddress, editTextAddress);
            editTextAddress.requestFocus();
        } else if (editTextOpeningTime.getText().toString().trim().isEmpty()) {
            editTextOpeningTime.getParent().requestChildFocus(editTextOpeningTime, editTextOpeningTime);
            editTextOpeningTime.requestFocus();
        } else if (editTextClosingTime.getText().toString().trim().isEmpty()) {
            editTextClosingTime.getParent().requestChildFocus(editTextClosingTime, editTextClosingTime);
            editTextClosingTime.requestFocus();
        } else if (idProofType == null) {
            layoutIdProof.getParent().requestChildFocus(layoutIdProof, layoutIdProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutIdProof);
        } else if (addressProofType == null) {
            layoutAddressProof.getParent().requestChildFocus(layoutAddressProof, layoutAddressProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutAddressProof);
        } else if (idProofPath == null) {
            imgViewIdProof.getParent().requestChildFocus(imgViewIdProof, imgViewIdProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(imgViewIdProof);
        } else if (addressProofPath == null) {
            imgViewAddressProof.getParent().requestChildFocus(imgViewAddressProof, imgViewAddressProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(imgViewAddressProof);
        } else if ( userProfile.client_type == 2 && businessProofType == null) {
            layoutBusinessProof.getParent().requestChildFocus(layoutBusinessProof, layoutBusinessProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutBusinessProof);
        } else if ( userProfile.client_type == 2 && businessProofPath == null) {
            imgViewBusinessProof.getParent().requestChildFocus(imgViewBusinessProof, imgViewBusinessProof);
            YoYo.with(Techniques.Shake).duration(1200).playOn(imgViewBusinessProof);
        } else {
            mPresenter.updateProfile(
                    editTextName.getText().toString(),
                    editTextMobile.getText().toString(),
                    editTextWorkExperience.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    serviceCityId,
                    editTextAddress.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextGstNo.getText().toString(),
                    latitude,
                    longitude,
                    changeTimeFormat(editTextOpeningTime.getText().toString()),
                    changeTimeFormat(editTextClosingTime.getText().toString()),
                    editTextDescription.getText().toString(),
                    filePath,
                    isNewImage,
                    addressProofPath,
                    addressProofType,
                    idProofPath,
                    idProofType,
                    businessProofPath,
                    businessProofType,
                    ac,
                    parking,
                    wifi,
                    music,
                    isNewIdProof,
                    isNewBusinessProof,
                    isNewAddressProof
            );
        }
    }

    @OnClick(R.id.btn_edit_profile_image) void onProfileEditClicked() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    @OnClick(R.id.img_id_proof) void onImageIdProofClicked() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_ID_PROOF);
    }

    @OnClick(R.id.img_address_proof) void onImageAddressProofClicked() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_ADDRESS_PROOF);
    }

    @OnClick(R.id.img_business_proof) void onImageBusinessProofClicked() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_BUSINESS_PROOF);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PIC:
                    if (data.getData() != null) {
                        filePath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(filePath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            filePath = null;
                        } else {
                            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, filePath, 150);
                            isNewImage = true;
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        filePath = saveImage(photo);
                        if (calculateFileSize(filePath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, filePath, 150);
                            isNewImage = true;
                        }
                    }
                    break;
                case REQUEST_ID_PROOF:
                    if (data.getData() != null) {
                        idProofPath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(idProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            idProofPath = null;
                        } else {
                            GlideHelper.setImageViewWithURI(this, imgViewIdProof, data.getData());
                            isNewIdProof = true;
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        idProofPath = saveImage(photo);
                        if (calculateFileSize(idProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            idProofPath = null;
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewIdProof, photo, 1);
                            isNewIdProof = true;
                        }
                    }
                    break;
                case REQUEST_ADDRESS_PROOF:
                    if (data.getData() != null) {
                        addressProofPath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(addressProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            addressProofPath = null;
                        } else {
                            GlideHelper.setImageViewWithURI(this, imgViewAddressProof, data.getData());
                            isNewAddressProof = true;
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        addressProofPath = saveImage(photo);
                        if (calculateFileSize(addressProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            addressProofPath = null;
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewAddressProof, photo, 1);
                            isNewAddressProof = true;
                        }
                    }
                    break;
                case REQUEST_BUSINESS_PROOF:
                    if (data.getData() != null) {
                        businessProofPath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(businessProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            businessProofPath = null;
                        } else {
                            GlideHelper.setImageViewWithURI(this, imgViewBusinessProof, data.getData());
                            isNewBusinessProof = true;
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        businessProofPath = saveImage(photo);
                        if (calculateFileSize(businessProofPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                            businessProofPath = null;
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewBusinessProof, photo, 1);
                            isNewBusinessProof = true;
                        }
                    }
                    break;
                case REQUEST_CHECK_SETTINGS_GPS:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            getLocation();
                            break;
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(getApplicationContext(), "Please enable GPS", Toast.LENGTH_SHORT).show();
                            break;
                    }
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0) {
                if (firstLoad) {
                    mPresenter.fetchUserProfile();
                    mPresenter.fetchCities();
                    firstLoad = false;
                }
            } else {
                if (firstLoad) {
                    mPresenter.fetchUserProfile();
                    mPresenter.fetchCities();
                    firstLoad = false;
                }
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("profilePath", filePath);
        savedInstanceState.putString("idProofPath", idProofPath);
        savedInstanceState.putString("addressProofPath", addressProofPath);
        savedInstanceState.putString("businessProofPath", businessProofPath);

        savedInstanceState.putBoolean("isNewImage", isNewImage);
        savedInstanceState.putBoolean("isNewAddressProof", isNewAddressProof);
        savedInstanceState.putBoolean("isNewBusinessProof", isNewBusinessProof);
        savedInstanceState.putBoolean("isNewIdProof", isNewIdProof);

    }

}