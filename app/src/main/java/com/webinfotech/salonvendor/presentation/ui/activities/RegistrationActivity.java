package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.RegisterUserPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.dialogs.SelectCityDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.TncDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity implements RegisterUserPresenter.View, SelectCityDialog.Callback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, TncDialog.Callback {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone_no)
    EditText editTextPhone;
    @BindView(R.id.radio_group_user_type)
    RadioGroup radioGroupUserType;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.txt_view_password_show)
    TextView txtViewPasswordShow;
    @BindView(R.id.txt_view_password_hide)
    TextView txtViewPasswordHide;
    @BindView(R.id.txt_view_confirm_password_hide)
    TextView txtViewConfirmPasswordHide;
    @BindView(R.id.txt_view_confirm_password_show)
    TextView txtViewConfirmPasswordShow;
    String gender;
    String otp;
    ProgressDialog progressDialog;
    RegisterUserPresenterImpl mPresenter;
    SelectCityDialog selectCityDialog;
    int cityID = 0;
    int userType = 0;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private double latitude;
    private double longitude;
    String[] appPremisions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1250;
    TncDialog tncDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(Html.fromHtml("<b>Sign Up</b>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (checkAndRequestPermissions()) {
            buildGoogleApiClient();
        }
        setUpProgressDialog();
        initialisePresenter();
        setSelectCityDialog();
        setRadioGroupGender();
        setTncDialog();
        mPresenter.fetchCityList();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterUserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(this, this, this);
        selectCityDialog.setUpDialogView();
    }

    private void setTncDialog() {
        tncDialog = new TncDialog(this, this, this);
        tncDialog.setUpDialogView();
    }

    private void setRadioGroupGender() {
        radioGroupUserType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
               if (checkedId == R.id.radio_btn_salon) {
                    userType = 2;
               } else {
                   userType = 1;
               }
            }
        });
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadCities(City[] cities) {
        selectCityDialog.setRecyclerView(cities);
    }

    @Override
    public void onUserRegistrationSuccess() {
        finish();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextConfirmPassword.getText().toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_LONG).show();
        } else if(editTextPassword.getText().toString().length() < 8) {
            Toast.makeText(this, "Password must be minimum 8 characters in length", Toast.LENGTH_LONG).show();
        } else if (!editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_LONG).show();
        } else if (userType == 0) {
            Toast.makeText(this, "Please Select User Type", Toast.LENGTH_LONG).show();
        } else if (cityID == 0) {
            Toast.makeText(this, "Please select a service city", Toast.LENGTH_LONG).show();
        } else {
            tncDialog.showDialog();
        }

    }

    @OnClick(R.id.layout_select_city) void onSelectCityClicked() {
        selectCityDialog.showDialog();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onCitySelected(int id, String cityName) {
        txtViewCity.setText(cityName);
        cityID = id;
        selectCityDialog.hideDialog();
    }

    @OnClick(R.id.txt_view_password_show) void onPasswordShowClicked() {
        editTextPassword.setTransformationMethod(null);
        txtViewPasswordShow.setVisibility(View.GONE);
        txtViewPasswordHide.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_view_password_hide) void onPasswordHideClicked() {
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
        txtViewPasswordShow.setVisibility(View.VISIBLE);
        txtViewPasswordHide.setVisibility(View.GONE);
    }

    @OnClick(R.id.txt_view_confirm_password_show) void onConfirmPasswordShowClicked() {
        editTextConfirmPassword.setTransformationMethod(null);
        txtViewConfirmPasswordShow.setVisibility(View.GONE);
        txtViewConfirmPasswordHide.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_view_confirm_password_hide) void onConfirmPasswordHideClicked() {
        editTextConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());
        txtViewConfirmPasswordShow.setVisibility(View.VISIBLE);
        txtViewConfirmPasswordHide.setVisibility(View.GONE);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE){
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                buildGoogleApiClient();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs location permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAgreeClicked() {
        mPresenter.registerUser(editTextName.getText().toString(), editTextPhone.getText().toString(), editTextPassword.getText().toString(), otp, userType, cityID, latitude, longitude);
    }
}