package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;

public interface BookingHistoryPresenter {
    void fetchBookingHistory();
    void cancelOrder(String reason);
    interface View {
        void loadAdapter(OrdersAdapter adapter);
        void goToMapLocation(double latitude, double longitude);
        void onCancelClicked();
        void showLoader();
        void hideLoader();
        void hideViews();
    }
}
