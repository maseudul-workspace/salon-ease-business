package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddServiceInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.AddServiceInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchThirdCategoryInteractorImpl;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.AddServicePresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class AddServicePresenterImpl extends AbstractPresenter implements   AddServicePresenter,
                                                                            FetchCategoriesInteractor.Callback,
                                                                            FetchSubcategoryInteractor.Callback,
                                                                            FetchThirdCategoryInteractor.Callback,
                                                                            AddServiceInteractor.Callback
{

    Context mContext;
    AddServicePresenter.View mView;

    public AddServicePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        FetchSubcategoryInteractorImpl fetchSubcategoryInteractor = new FetchSubcategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoryInteractor.execute();
    }

    @Override
    public void fetchThirdCategories(int categoryId) {
        FetchThirdCategoryInteractorImpl fetchThirdCategoryInteractor = new FetchThirdCategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchThirdCategoryInteractor.execute();
    }

    @Override
    public void addService(ArrayList<Integer> mainCategoryIds, ArrayList<Integer> subCategoryIds, ArrayList<Integer> lastCategoryIds, ArrayList<String> mrp, ArrayList<String> price) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            AddServiceInteractorImpl addServiceInteractor = new AddServiceInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userDetails.api_token, userDetails.id, mainCategoryIds, subCategoryIds, lastCategoryIds, mrp, price);
            addServiceInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadCategories(categories);
    }

    @Override
    public void onGettingCategoriesFail() {

    }

    @Override
    public void onGettingSubcategorySuccess(Subcategory[] subcategories) {
        mView.loadSubcategories(subcategories);
    }

    @Override
    public void ongGettingSubcategoryFail(String errorMsg) {

    }

    @Override
    public void onGettingThirdCategorySuccess(ThirdCategory[] thirdCategories) {
        mView.loadThirdCategories(thirdCategories);
    }

    @Override
    public void ongGettingThirdCategoryFail(String errorMsg) {

    }

    @Override
    public void onServiceAddSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Services Added", Toast.LENGTH_SHORT).show();
        mView.onServiceAddSuccess();
    }

    @Override
    public void onServiceAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
