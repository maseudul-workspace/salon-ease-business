package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.UserProfile;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void submitFile(String filePath);
    void deleteImage(int id);
    void updateSchedule(String date, int status);
    void fetchCategories();
    void addService(int jobId, int isWomen, String womanMrp, String womanPrice, int isMan, String manMrp, String manPrice, int isKids, String kidsMrp, String kidsPrice, String description);
    void updateService(int serviceId, String mrp, String price, int mainCategoryId, int subCategoryId, int lastCategoryId);
    void updateServiceStatus(int serviceId, int status);
    interface View {
        void loadUserProfile(UserProfile userProfile);
        void onFileUploadSuccess();
        void onImageDeleteSuccess();
        void loadCategories(Category[] categories);
        void showLoader();
        void hideLoader();
    }
}
