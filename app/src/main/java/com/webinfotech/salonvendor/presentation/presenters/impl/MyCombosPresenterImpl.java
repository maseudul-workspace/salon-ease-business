package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.EditComboInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCombosInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateServiceStatusInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.EditComboInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCombosInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateServiceStatusInteractorImpl;
import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.domain.models.ComboServiceData;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.MyCombosPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.presentation.ui.adapter.CombosAdapter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class MyCombosPresenterImpl extends AbstractPresenter implements MyCombosPresenter,
                                                                        FetchCombosInteractor.Callback,
                                                                        CombosAdapter.Callback,
                                                                        EditComboInteractor.Callback,
                                                                        UpdateServiceStatusInteractor.Callback
{

    Context mContext;
    MyCombosPresenter.View mView;
    ComboServiceData[] comboServiceData;
    int mainCategoryId;
    int comboId;
    int serviceId;
    String comboName;

    public MyCombosPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCombos() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchCombosInteractorImpl fetchCombosInteractor = new FetchCombosInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token);
            fetchCombosInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void editCombo(String serviceName, String mrp, String price) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            ArrayList<String> serviceNames = new ArrayList<>();
            ArrayList<String> mrps = new ArrayList<>();
            ArrayList<String> prices = new ArrayList<>();
            ArrayList<Integer> serviceIds = new ArrayList<>();
            for (ComboServiceData comboServiceDatum : comboServiceData) {
                if (comboServiceDatum.id == comboId) {
                    comboName = comboServiceDatum.comboName;
                    mainCategoryId = comboServiceDatum.categoryId;
                    ComboService[] comboServices = comboServiceDatum.comboServices;
                    for (ComboService comboService : comboServices) {
                        if (comboService.id == serviceId) {
                            serviceNames.add(serviceName);
                            mrps.add(mrp);
                            prices.add(price);
                        } else {
                            serviceNames.add(comboService.name);
                            mrps.add(comboService.mrp);
                            prices.add(comboService.price);
                        }
                        serviceIds.add(comboService.id);
                    }
                    break;
                }
            }
            EditComboInteractorImpl editComboInteractor = new EditComboInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, comboName, mainCategoryId, serviceNames, mrps, prices, comboId, serviceIds);
            editComboInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingCombosSuccess(ComboServiceData[] comboServiceData) {
        this.comboServiceData = comboServiceData;
        CombosAdapter combosAdapter = new CombosAdapter(mContext, comboServiceData, this);
        mView.loadAdapter(combosAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingCombosFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onEditClicked(int comboId, int serviceId) {
        this.comboId = comboId;
        this.serviceId = serviceId;
        for (int i = 0; i < comboServiceData.length; i++) {
            if (comboServiceData[i].id == comboId) {
                ComboService[] comboServices = comboServiceData[i].comboServices;
                for (int j = 0 ; j < comboServices.length; j++) {
                    if (comboServices[j].id == serviceId) {
                        mView.setData(comboServices[j]);
                        break;
                    }
                }
                break;
            }
        }
    }

    @Override
    public void updateServiceStatus(int serviceId, int status) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UpdateServiceStatusInteractorImpl updateServiceStatusInteractor = new UpdateServiceStatusInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, serviceId, status);
            updateServiceStatusInteractor.execute();
        }
    }

    @Override
    public void onComboEditSuccess() {
        fetchCombos();
    }

    @Override
    public void onComboEditFail(int loginError, String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onServiceUpdateSuccess() {

    }

    @Override
    public void onServiceUpdateFail(String errorMsg, int loginError) {

    }
}
