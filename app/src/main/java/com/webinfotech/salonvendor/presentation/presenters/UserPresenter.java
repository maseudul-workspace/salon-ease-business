package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.UserProfile;

public interface UserPresenter {
    void fetchUserProfile();
    interface View {
        void loadUserProfile(UserProfile userProfile);
        void showLoader();
        void hideLoader();
    }
}
