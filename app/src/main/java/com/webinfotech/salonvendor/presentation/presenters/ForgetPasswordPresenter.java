package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.City;

public interface ForgetPasswordPresenter {
    void requestPasswordChange(String mobile);
    void resetPassword(String phone, String otp, String password);
    interface View {
        void onPasswordRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
