package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.ChangeOrderStatusInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.ChangeOrderStatusInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateFirebaseInteractorImpl;
import com.webinfotech.salonvendor.domain.models.Orders;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.MainPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchUserProfileInteractor.Callback,
                                                                    UpdateFirebaseInteractor.Callback
{

    Context mContext;
    MainPresenter.View mView;
    int orderId;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchUserProfileInteractorImpl fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchUserProfileInteractor.execute();
        }
    }

    @Override
    public void updateFirebaseToken(String token) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails == null) {
        } else {
            UpdateFirebaseInteractorImpl updateFirebaseInteractor = new UpdateFirebaseInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userDetails.api_token, userDetails.id, token);
            updateFirebaseInteractor.execute();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserProfile userProfile) {
        mView.loadUserProfile(userProfile);
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {

    }

    @Override
    public void onFirebaseTokenUpdateSuccess() {

    }

    @Override
    public void onFirebaseTokenUpdateFail() {

    }
}
