package com.webinfotech.salonvendor.presentation.presenters;

public interface RegistrationOtpVerifyPresenter {
    void sendOtp(String phone);
    interface View {
        void onOtpSendSuccess(String otp);
        void showLoader();
        void hideLoader();
    }
}
