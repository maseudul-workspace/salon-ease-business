package com.webinfotech.salonvendor.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.presentation.ui.adapter.CategoryAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesListDialog implements CategoryAdapter.Callback {

    @Override
    public void onCategoryClicked(int position) {
        hideDialog();
        mCallback.onCategorySelected(position);
    }

    public interface Callback {
        void onCategorySelected(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_categories)
    RecyclerView recyclerViewCategories;

    public CategoriesListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.category_list_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setCategories(Category[] categories) {
        CategoryAdapter adapter = new CategoryAdapter(mContext, categories, this);
        recyclerViewCategories.setAdapter(adapter);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewCategories.addItemDecoration(new DividerItemDecoration(recyclerViewCategories.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
