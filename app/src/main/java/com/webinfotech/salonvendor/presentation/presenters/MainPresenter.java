package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.ui.adapter.OrdersAdapter;

public interface MainPresenter {
    void fetchUserProfile();
    void updateFirebaseToken(String token);
    interface View {
        void loadUserProfile(UserProfile userProfile);
        void showLoader();
        void hideLoader();
    }
}
