package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.ForgetPasswordOtpVerifyPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.ForgetPasswordOtpVerifyPresenterImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class ForgetPasswordOtpActivity extends AppCompatActivity implements ForgetPasswordOtpVerifyPresenter.View {

    ForgetPasswordOtpVerifyPresenterImpl mPresenter;
    @BindView(R.id.edit_text_phone_no)
    EditText editTextPhone;
    @BindView(R.id.layout_otp)
    View layoutOtp;
    @BindView(R.id.layout_phone_no)
    View layoutPhone;
    @BindView(R.id.pinview)
    Pinview pinview;
    String otp;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_otp);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Verify Phone No");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ForgetPasswordOtpVerifyPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please Enter Your 10 Digits Phone No", Toast.LENGTH_LONG).show();
        } else if (editTextPhone.getText().toString().trim().length() < 10) {
            Toast.makeText(this, "Phone No Must Be 10 Digits", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.sendOtp(editTextPhone.getText().toString());
        }
    }

    @OnClick(R.id.btn_check) void onOtpCheckClicked() {
        if (pinview.getValue().length() < 5) {
            Toast.makeText(this, "OTP should be 5 digit", Toast.LENGTH_SHORT).show();
        } else {
            if (pinview.getValue().toString().equals(otp)) {
                Intent intent = new Intent(this, ForgetPasswordActivity.class);
                intent.putExtra("phoneNo", editTextPhone.getText().toString());
                intent.putExtra("otp", otp);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Wrong Otp", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onOtpSendSuccess(String otp) {
        this.otp = otp;
        layoutPhone.setVisibility(View.GONE);
        layoutOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}