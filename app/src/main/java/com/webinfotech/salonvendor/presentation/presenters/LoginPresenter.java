package com.webinfotech.salonvendor.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String phone, String password);
    interface View {
        void onLoginSuccess();
        void showLoader();
        void hideLoader();
    }
}
