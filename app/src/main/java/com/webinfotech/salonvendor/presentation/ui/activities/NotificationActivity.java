package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.NotificationPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.NotificationPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.NotificationListRecyclerViewAdapter;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class NotificationActivity extends AppCompatActivity implements NotificationPresenter.View {

    @BindView(R.id.recycler_view_message_list)
    RecyclerView recyclerViewMessageList;

    NotificationPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    private ProgressDialog progressDialog;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Notification");
        initialsiePresenter();
        setUpProgressDialog();
        setBottomNavigationView();
        mPresenter.fetchMessageList(pageNo, "refresh");
    }

    private void initialsiePresenter() {
        mPresenter = new NotificationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setSelectedItemId(R.id.nav_notification);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_profile:
                        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_bookings:
                        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainIntent);
                        break;
                    case R.id.nav_about:
                        Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(aboutIntent);
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public void loadAdapter(NotificationListRecyclerViewAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewMessageList.setLayoutManager(layoutManager);
        recyclerViewMessageList.setAdapter(adapter);
        recyclerViewMessageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchMessageList(pageNo,"");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onLoginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}