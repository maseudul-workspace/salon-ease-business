package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.AddedService;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;
import com.webinfotech.salonvendor.presentation.presenters.AddServicePresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.AddServicePresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.AddedServicesAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.CategoriesListDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.SubcategoriesListDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.ThirdCategoryListDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

import java.util.ArrayList;

public class AddServiceActivity extends AppCompatActivity implements AddServicePresenter.View, CategoriesListDialog.Callback, SubcategoriesListDialog.Callback, ThirdCategoryListDialog.Callback {

    @BindView(R.id.recycler_view_services)
    RecyclerView recyclerViewServices;
    @BindView(R.id.layout_buttons)
    View layoutButtons;
    @BindView(R.id.btn_add_service)
    Button btnAddService;
    ArrayList<AddedService> addedServices = new ArrayList<>();
    BottomSheetDialog addServiceDialog;
    CategoriesListDialog categoriesListDialog;
    SubcategoriesListDialog subcategoriesListDialog;
    ThirdCategoryListDialog thirdCategoryListDialog;
    Category[] categories;
    Subcategory[] subcategories;
    ThirdCategory[] thirdCategories;
    Category selectedCategory = null;
    Subcategory selectedSubcategory = null;
    ThirdCategory selectedThirdcategory = null;
    AddServicePresenterImpl mPresenter;
    EditText editTextMainCategory;
    EditText editTextSecondCategory;
    EditText editTextThirdCategory;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        getSupportActionBar().setTitle("Add Service");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setDialogs();
        setAddServiceDialog();
        showServiceAddBottomsheet();
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchCategories();
    }

    private void initialisePresenter() {
        mPresenter = new AddServicePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setDialogs() {

        categoriesListDialog = new CategoriesListDialog(this, this, this);
        categoriesListDialog.setUpDialogView();

        subcategoriesListDialog = new SubcategoriesListDialog(this, this, this);
        subcategoriesListDialog.setUpDialogView();

        thirdCategoryListDialog = new ThirdCategoryListDialog(this, this, this);
        thirdCategoryListDialog.setUpDialogView();

    }

    private void setAddServiceDialog () {
        if (addServiceDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_add_service_bottom_sheet, null);
            addServiceDialog = new BottomSheetDialog(this);
            editTextMainCategory = (EditText) view.findViewById(R.id.edit_text_main_category);
            editTextSecondCategory = (EditText) view.findViewById(R.id.edit_text_second_category);
            editTextThirdCategory = (EditText) view.findViewById(R.id.edit_text_third_category);
            EditText editTextMrp = (EditText) view.findViewById(R.id.edit_text_mrp);
            EditText editTextPrice = (EditText) view.findViewById(R.id.edit_text_price);
            Button btnSubnit = (Button) view.findViewById(R.id.btn_submit);

            editTextMainCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoriesListDialog.showDialog();
                }
            });

            editTextSecondCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subcategoriesListDialog.showDialog();
                }
            });

            editTextThirdCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    thirdCategoryListDialog.showDialog();
                }
            });

            btnSubnit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedCategory == null) {
                        Toast.makeText(getApplicationContext(), "Please select a category", Toast.LENGTH_SHORT).show();
                    } else if (selectedSubcategory == null) {
                        Toast.makeText(getApplicationContext(), "Please select a subcategory", Toast.LENGTH_SHORT).show();
                    } else if (selectedThirdcategory == null) {
                        Toast.makeText(getApplicationContext(), "Please select a third category", Toast.LENGTH_SHORT).show();
                    } else if (editTextMrp.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter mrp", Toast.LENGTH_SHORT).show();
                    } else if (editTextPrice.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter price", Toast.LENGTH_SHORT).show();
                    } else {
                        int flag = 0;
                        for (int i = 0; i < addedServices.size(); i++) {
                            if (addedServices.get(i).thirdCategoryId == selectedThirdcategory.id) {
                                Toast.makeText(getApplicationContext(), "Service already added to the list, Please add another service", Toast.LENGTH_LONG).show();
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 0) {
                            addedServices.add(new AddedService(selectedCategory.id, selectedCategory.name, selectedCategory.image, selectedSubcategory.id, selectedSubcategory.name, selectedSubcategory.image, selectedThirdcategory.id, selectedThirdcategory.name, selectedThirdcategory.image, editTextMrp.getText().toString(), editTextPrice.getText().toString()));
                            selectedCategory = null;
                            selectedSubcategory = null;
                            selectedThirdcategory = null;
                            editTextMrp.setText("");
                            editTextPrice.setText("");
                            editTextMainCategory.setText("");
                            editTextSecondCategory.setText("");
                            editTextThirdCategory.setText("");
                            hideServiceAddBottomsheet();
                            setRecyclerViewServices();
                        }
                    }
                }
            });
            addServiceDialog.setContentView(view);
        }
    }

    @Override
    public void onCategorySelected(int position) {
        editTextMainCategory.setText(categories[position].name);
        mPresenter.fetchSubcategories(categories[position].id);
        selectedCategory = categories[position];
    }

    @Override
    public void onSubcategoryClicked(int position) {
        editTextSecondCategory.setText(subcategories[position].name);
        mPresenter.fetchThirdCategories(subcategories[position].id);
        selectedSubcategory = subcategories[position];
    }

    @Override
    public void onThirdCategorySelected(int position) {
        editTextThirdCategory.setText(thirdCategories[position].name);
        selectedThirdcategory = thirdCategories[position];
    }

    @Override
    public void loadCategories(Category[] categories) {
        categoriesListDialog.setCategories(categories);
        this.categories = categories;
    }

    @Override
    public void loadSubcategories(Subcategory[] subcategories) {
        subcategoriesListDialog.setCategories(subcategories);
        this.subcategories = subcategories;
    }

    @Override
    public void loadThirdCategories(ThirdCategory[] thirdCategories) {
        thirdCategoryListDialog.setCategories(thirdCategories);
        this.thirdCategories = thirdCategories;
    }

    @Override
    public void onServiceAddSuccess() {
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    private void showServiceAddBottomsheet() {
        addServiceDialog.show();
    }

    private void hideServiceAddBottomsheet() {
        addServiceDialog.dismiss();
    }

    private void setRecyclerViewServices() {
        AddedServicesAdapter adapter = new AddedServicesAdapter(this, addedServices);
        recyclerViewServices.setAdapter(adapter);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(this));
        layoutButtons.setVisibility(View.VISIBLE);
        btnAddService.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_add_more) void onAddMoreClicked() {
        showServiceAddBottomsheet();
    }

    @OnClick(R.id.btn_confirm) void onConfirmClicked() {
        ArrayList<Integer> mainCategoryIds = new ArrayList<>();
        ArrayList<Integer> secondCategoryIds = new ArrayList<>();
        ArrayList<Integer> lastCategoryIds = new ArrayList<>();
        ArrayList<String> mrp = new ArrayList<>();
        ArrayList<String> price = new ArrayList<>();

        for (int i = 0; i < addedServices.size(); i++) {
            mainCategoryIds.add(addedServices.get(i).mainCategoryId);
            secondCategoryIds.add(addedServices.get(i).subCategoryId);
            lastCategoryIds.add(addedServices.get(i).thirdCategoryId);
            mrp.add(addedServices.get(i).mrp);
            price.add(addedServices.get(i).price);
        }

        mPresenter.addService(mainCategoryIds, secondCategoryIds, lastCategoryIds, mrp, price);

    }

    @OnClick(R.id.btn_add_service) void onAddServiceClicked() {
        addServiceDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}