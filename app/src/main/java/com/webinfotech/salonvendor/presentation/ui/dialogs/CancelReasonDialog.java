package com.webinfotech.salonvendor.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.salonvendor.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CancelReasonDialog {

    public interface Callback {
        void onCancelClicked(String reason);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.spinner_cancel_reasons)
    Spinner spinnerCancelReasons;
    String reason = null;

    public CancelReasonDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_cancel_reason_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setSpinnerCancelReasons();
    }

    private void setSpinnerCancelReasons() {
        final ArrayAdapter<String> reasonsAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_text_layout, mContext.getResources().getStringArray(R.array.cancel_reasons)){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerCancelReasons.setAdapter(reasonsAdapter);
        spinnerCancelReasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    reason = mContext.getResources().getStringArray(R.array.cancel_reasons)[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (reason == null) {
            Toast.makeText(mContext, "Please specify a reason", Toast.LENGTH_SHORT).show();
        } else {
            mCallback.onCancelClicked(reason);
        }
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

}
