package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.ChangePasswordInteractor;
import com.webinfotech.salonvendor.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.ChangePasswordInteractorImpl;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class ChangePasswordPresenterImpl extends AbstractPresenter implements ChangePasswordPresenter, ChangePasswordInteractor.Callback {

    Context mContext;
    ChangePasswordPresenter.View mView;

    public ChangePasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void changePassword(String currentPassword, String newPassword) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            ChangePasswordInteractorImpl changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id, currentPassword, newPassword);
            changePasswordInteractor.execute();
            mView.showLoader();
        } else {

        }
    }

    @Override
    public void onPasswordChangeSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Password Changed", Toast.LENGTH_SHORT).show();
        mView.onPasswordChangedSuccess();
    }

    @Override
    public void onPasswordChangeFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
