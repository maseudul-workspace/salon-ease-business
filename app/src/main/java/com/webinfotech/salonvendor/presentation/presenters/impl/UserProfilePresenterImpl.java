package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddServiceInteractor;
import com.webinfotech.salonvendor.domain.interactors.DeleteGalleryImageInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateScheduleInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateServiceInteractor;
import com.webinfotech.salonvendor.domain.interactors.UpdateServiceStatusInteractor;
import com.webinfotech.salonvendor.domain.interactors.UploadImageInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.AddServiceInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.DeleteGalleryImageInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateScheduleInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateServiceInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UpdateServiceStatusInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.UploadImageIntercatorImpl;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class UserProfilePresenterImpl extends AbstractPresenter implements  UserProfilePresenter,
                                                                            UploadImageInteractor.Callback,
                                                                            FetchUserProfileInteractor.Callback,
                                                                            DeleteGalleryImageInteractor.Callback,
                                                                            UpdateScheduleInteractor.Callback,
                                                                            FetchCategoriesInteractor.Callback,
                                                                            AddServiceInteractor.Callback,
                                                                            UpdateServiceInteractor.Callback,
                                                                            UpdateServiceStatusInteractor.Callback
{

    Context mContext;
    UserProfilePresenter.View mView;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchUserProfileInteractorImpl fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        } else {

        }
    }

    @Override
    public void submitFile(String filePath) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UploadImageIntercatorImpl uploadImageIntercator = new UploadImageIntercatorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id, filePath);
            uploadImageIntercator.execute();
            mView.showLoader();
        }
    }

    @Override
    public void deleteImage(int id) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            DeleteGalleryImageInteractorImpl deleteGalleryImageInteractor = new DeleteGalleryImageInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userDetails.api_token, userDetails.id, id);
            deleteGalleryImageInteractor.execute();
            mView.showLoader();
        } else {

        }
    }

    @Override
    public void updateSchedule(String date, int status) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UpdateScheduleInteractorImpl updateScheduleInteractor = new UpdateScheduleInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userDetails.api_token, userDetails.id, date, status);
            updateScheduleInteractor.execute();
        }
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
    }

    @Override
    public void addService(int jobId, int isWomen, String womanMrp, String womanPrice, int isMan, String manMrp, String manPrice, int isKids, String kidsMrp, String kidsPrice, String description) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {

        }
    }

    @Override
    public void updateService(int serviceId, String mrp, String price, int mainCategoryId, int subCategoryId, int lastCategoryId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UpdateServiceInteractorImpl updateServiceInteractor = new UpdateServiceInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, serviceId, mrp, price, mainCategoryId, subCategoryId, lastCategoryId);
            updateServiceInteractor.execute();
            mView.showLoader();
        } else {

        }
    }

    @Override
    public void updateServiceStatus(int serviceId, int status) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            UpdateServiceStatusInteractorImpl updateServiceStatusInteractor = new UpdateServiceStatusInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, serviceId, status);
            updateServiceStatusInteractor.execute();
        } else {

        }
    }

    @Override
    public void onFileUploadSuccess() {
        fetchUserProfile();
        Toast.makeText(mContext, "File Uploaded Successfully", Toast.LENGTH_SHORT).show();
        mView.onFileUploadSuccess();
    }

    @Override
    public void onFileUploadFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingUserProfileSuccess(UserProfile userProfile) {
        mView.loadUserProfile(userProfile);
        mView.hideLoader();
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onImageDeleteSuccess() {
        Toast.makeText(mContext, "Image Removed", Toast.LENGTH_SHORT).show();
        fetchUserProfile();
    }

    @Override
    public void onImageDeleteFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onScheduleUpdateSuccess() {
    }

    @Override
    public void onScheduleUpdateFail(String errorMsg, int loginError) {
    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadCategories(categories);
    }

    @Override
    public void onGettingCategoriesFail() {

    }

    @Override
    public void onServiceAddSuccess() {
        mView.hideLoader();
        fetchUserProfile();
    }

    @Override
    public void onServiceAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateServiceSuccess() {
        fetchUserProfile();
    }

    @Override
    public void onUpdateServiceFailed(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceUpdateSuccess() {
    }

    @Override
    public void onServiceUpdateFail(String errorMsg, int loginError) {
    }
}