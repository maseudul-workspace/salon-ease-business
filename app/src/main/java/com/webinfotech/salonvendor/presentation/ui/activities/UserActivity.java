package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.UserPresenter;
import com.webinfotech.salonvendor.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.UserPresenterImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;
import com.webinfotech.salonvendor.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserActivity extends AppCompatActivity implements UserPresenter.View {

    @BindView(R.id.img_view_user)
    ImageView imgViewUserProfile;
    UserPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.txt_view_user)
    TextView txtViewUser;
    String[] appPremisions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        getSupportActionBar().setTitle(Html.fromHtml("<b>My Profile</b>"));
        ButterKnife.bind(this);
        setBottomNavigationView();
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new UserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setSelectedItemId(R.id.nav_profile);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_bookings:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_about:
                        Intent aboutIntent = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(aboutIntent);
                        break;
                    case R.id.nav_notification:
                        Intent notificationIntent = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(notificationIntent);
                        break;
                    case R.id.nav_contact_us:
                        Intent contactusIntent = new Intent(getApplicationContext(), ContactusActivity.class);
                        startActivity(contactusIntent);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void loadUserProfile(UserProfile userProfile) {
        if (userProfile.image != null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imgViewUserProfile, getResources().getString(R.string.base_url) + "images/client/" + userProfile.image, 150);
        }
        txtViewUser.setText(userProfile.name);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.view_user_profile) void onUserProfileClicked() {
        if (checkAndRequestPermissions()) {
            Intent intent = new Intent(this, UserProfileActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "All Permissions Are Required. Please Provide All The Permissions", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.view_change_password) void onChangePasswordActivity() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_log_out) void onLogOutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_add_deal) void onAddDealClicked() {
        Intent intent = new Intent(this, MyDealsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_my_combos) void onMyCombosClicked() {
        Intent intent = new Intent(this, MyCombosActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchUserProfile();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0) {
                Intent intent = new Intent(this, UserProfileActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "All Permissions Are Required. Please Provide All The Permissions", Toast.LENGTH_LONG).show();
            }

        }
    }



}