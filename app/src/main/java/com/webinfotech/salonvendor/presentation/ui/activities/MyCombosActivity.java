package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.presentation.presenters.MyCombosPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.MyCombosPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.CombosAdapter;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

import java.security.PrivateKey;

public class MyCombosActivity extends AppCompatActivity implements MyCombosPresenter.View {

    @BindView(R.id.btn_add_combo)
    ExtendedFloatingActionButton btnAddCombo;
    @BindView(R.id.recycler_view_combos)
    RecyclerView recyclerViewCombos;
    ProgressDialog progressDialog;
    MyCombosPresenterImpl mPresenter;
    BottomSheetDialog addComboDialog;
    private EditText editTextPrice;
    private EditText editTextMrp;
    private EditText editTextServiceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_combos);
        ButterKnife.bind(this);
        setUpProgressDialog();
        getSupportActionBar().setTitle("My Combos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnAddCombo.shrink();
        initialisePresenter();
        setAddComboDialog();
    }

    private void initialisePresenter() {
        mPresenter = new MyCombosPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setAddComboDialog() {
        View view  = LayoutInflater.from(this).inflate(R.layout.layout_add_combo_service_bottomsheets, null);
        addComboDialog = new BottomSheetDialog(this);
        addComboDialog.setContentView(view);
        editTextPrice = (EditText) view.findViewById(R.id.edit_text_price);
        editTextMrp = (EditText) view.findViewById(R.id.edit_text_mrp);
        editTextServiceName = (EditText) view.findViewById(R.id.edit_text_service_name);
        Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);
        btnSubmit.setText("Submit");

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextServiceName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Service Name", Toast.LENGTH_SHORT).show();
                } else if (editTextMrp.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Mrp", Toast.LENGTH_SHORT).show();
                } else if (editTextPrice.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Price", Toast.LENGTH_SHORT).show();
                } else {
                    addComboDialog.dismiss();
                    mPresenter.editCombo(editTextServiceName.getText().toString(), editTextMrp.getText().toString(), editTextPrice.getText().toString());
                }
            }
        });

    }

    @OnClick(R.id.btn_add_combo) void onAddComboClicked() {
        if (btnAddCombo.isExtended()) {
            btnAddCombo.shrink();
            goToAddComboActivity();
        } else {
            btnAddCombo.extend();
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void goToAddComboActivity() {
        Intent intent = new Intent(this, AddComboActivity.class);
        startActivity(intent);
    }

    @Override
    public void loadAdapter(CombosAdapter combosAdapter) {
        recyclerViewCombos.setAdapter(combosAdapter);
        recyclerViewCombos.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void setData(ComboService comboService)  {
        editTextMrp.setText(comboService.mrp);
        editTextPrice.setText(comboService.price);
        editTextServiceName.setText(comboService.name);
        addComboDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCombos();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}