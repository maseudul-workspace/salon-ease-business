package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.presentation.ui.adapter.CombosAdapter;

public interface MyCombosPresenter {
    void fetchCombos();
    void editCombo(String serviceName, String mrp, String price);
    interface View {
        void loadAdapter(CombosAdapter combosAdapter);
        void setData(ComboService comboService);
        void showLoader();
        void hideLoader();
    }
}
