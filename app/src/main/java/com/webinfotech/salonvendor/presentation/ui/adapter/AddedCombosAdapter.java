package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.AddedCombo;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddedCombosAdapter extends RecyclerView.Adapter<AddedCombosAdapter.ViewHolder> {

    Context mContext;
    ArrayList<AddedCombo> addedCombos;

    public AddedCombosAdapter(Context mContext, ArrayList<AddedCombo> addedCombos) {
        this.mContext = mContext;
        this.addedCombos = addedCombos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_added_combos, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewMainCategory.setText(addedCombos.get(position).comboName);
        holder.txtViewMrp.setText("Rs. " + addedCombos.get(position).mrp);
        holder.txtViewPrice.setText("Rs. " + addedCombos.get(position).price);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return addedCombos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_subcategory)
        TextView txtViewMainCategory;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
