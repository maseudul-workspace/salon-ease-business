package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.ServiceOrder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderServicesAdapter extends RecyclerView.Adapter<OrderServicesAdapter.ViewHolder> {

    Context mContext;
    ServiceOrder[] serviceOrders;

    public OrderServicesAdapter(Context mContext, ServiceOrder[] serviceOrders) {
        this.mContext = mContext;
        this.serviceOrders = serviceOrders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewService.setText(serviceOrders[position].categoryName + " -> " + serviceOrders[position].subCategoryName + " -> " + serviceOrders[position].thirdCategoryName);
        holder.txtViewPrice.setText("Rs. " + serviceOrders[position].amount);
    }

    @Override
    public int getItemCount() {
        return serviceOrders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_service)
        TextView txtViewService;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
