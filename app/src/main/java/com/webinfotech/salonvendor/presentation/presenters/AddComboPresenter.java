package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.Category;

import java.util.ArrayList;

public interface AddComboPresenter {
    void fetchCategories();
    void addCombo(String comboName, int mainCategoryId, ArrayList<String>serviceNames, ArrayList<String> mrp, ArrayList<String> price);
    interface View {
        void loadCategories(Category[] categories);
        void showLoader();
        void hideLoader();
        void onAddComboSuccess();
    }
}
