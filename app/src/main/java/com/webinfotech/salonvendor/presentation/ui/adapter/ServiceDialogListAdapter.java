package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceDialogListAdapter extends RecyclerView.Adapter<ServiceDialogListAdapter.ViewHolder> {

    public interface Callback {
        void onServiceClicked(int clientServiceId, String serviceName);
    }

    Context mContext;
    Service[] services;
    Callback mCallback;

    public ServiceDialogListAdapter(Context mContext, Service[] services, Callback mCallback) {
        this.mContext = mContext;
        this.services = services;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_services_dialog_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewMainCategory.setText(services[position].mainCategory + " -> " + services[position].subCategory);
        holder.txtViewSubcategory.setText(services[position].lastCategory);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewMainCategoryIcon, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + services[position].lastCategoryImage, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onServiceClicked(services[position].id, services[position].lastCategory);
            }
        });
    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_category)
        ImageView imgViewMainCategoryIcon;
        @BindView(R.id.txt_view_main_category)
        TextView txtViewMainCategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
