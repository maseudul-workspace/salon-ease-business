package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddComboInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.AddComboInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.AddComboPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class AddComboPresenterImpl extends AbstractPresenter implements AddComboPresenter,
                                                                        FetchCategoriesInteractor.Callback,
                                                                        AddComboInteractor.Callback
{

    Context mContext;
    AddComboPresenter.View mView;

    public AddComboPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void addCombo(String comboName, int mainCategoryId, ArrayList<String> serviceNames, ArrayList<String> mrp, ArrayList<String> price) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            AddComboInteractorImpl addComboInteractor = new AddComboInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, comboName, mainCategoryId, serviceNames, mrp, price);
            addComboInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadCategories(categories);
        mView.hideLoader();
    }

    @Override
    public void onGettingCategoriesFail() {
        mView.hideLoader();
    }

    @Override
    public void onComboAddSuccess() {
        mView.hideLoader();
        mView.onAddComboSuccess();
    }

    @Override
    public void onComboAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

}
