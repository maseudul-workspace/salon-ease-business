package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.ComboService;
import com.webinfotech.salonvendor.domain.models.ComboServiceData;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CombosAdapter extends RecyclerView.Adapter<CombosAdapter.ViewHolder> implements ComboServicesAdapter.Callback{

    @Override
    public void onEditClicked(int comboServiceId, int serviceId) {
        mCallback.onEditClicked(comboServiceId, serviceId);
    }

    public interface Callback {
        void onEditClicked(int comboId, int serviceId);
        void updateServiceStatus(int serviceId, int status);
    }

    Context mContext;
    ComboServiceData[] comboServiceData;
    Callback mCallback;

    public CombosAdapter(Context mContext, ComboServiceData[] comboServices, Callback mCallback) {
        this.mContext = mContext;
        this.comboServiceData = comboServices;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_combos, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboName.setText(comboServiceData[position].comboName);
        holder.txtViewMrp.setText("Rs. " + comboServiceData[position].mrp);
        holder.txtViewPrice.setText("Rs. " + comboServiceData[position].price);
        ComboServicesAdapter adapter = new ComboServicesAdapter(mContext, comboServiceData[position].comboServices, this);
        holder.recyclerViewComboServices.setAdapter(adapter);
        holder.recyclerViewComboServices.setLayoutManager(new LinearLayoutManager(mContext));
        holder.switchAvailability.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.updateServiceStatus(comboServiceData[position].id, 1);
                } else {
                    mCallback.updateServiceStatus(comboServiceData[position].id, 2);
                }
            }
        });
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        if (comboServiceData[position].status == 1) {
            holder.switchAvailability.setChecked(true);
            holder.switchAvailability.setText("Available");
        } else {
            holder.switchAvailability.setChecked(false);
            holder.switchAvailability.setText("Not Available");
        }
    }

    @Override
    public int getItemCount() {
        return comboServiceData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_combo_name)
        TextView txtViewComboName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.recycler_view_combo_services)
        RecyclerView recyclerViewComboServices;
        @BindView(R.id.switch_availability)
        SwitchMaterial switchAvailability;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
