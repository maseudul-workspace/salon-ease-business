package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.domain.models.UserProfile;
import com.webinfotech.salonvendor.presentation.presenters.UserPresenter;
import com.webinfotech.salonvendor.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class UserPresenterImpl extends AbstractPresenter implements UserPresenter,
                                                                    FetchUserProfileInteractor.Callback {

    Context mContext;
    UserPresenter.View mView;

    public UserPresenterImpl(Executor executor, MainThread mainThread, Context mContext, UserPresenter.View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchUserProfileInteractorImpl fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingUserProfileSuccess(UserProfile userProfile) {
        mView.hideLoader();
        mView.loadUserProfile(userProfile);
    }

    @Override
    public void onGettingUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
