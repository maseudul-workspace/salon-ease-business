
package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.AddedCombo;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.presentation.presenters.AddComboPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.AddComboPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.AddedCombosAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.CategoriesListDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.List;

public class AddComboActivity extends AppCompatActivity implements AddComboPresenter.View, CategoriesListDialog.Callback {

    BottomSheetDialog addComboDialog;
    EditText editTextServiceName;
    EditText editTextMrp;
    EditText editTextPrice;
    @BindView(R.id.txt_view_select_category)
    TextView txtViewSelectCategory;
    @BindView(R.id.edit_text_combo_name)
    EditText editTextComboName;
    @BindView(R.id.recycler_view_added_combos)
    RecyclerView recyclerViewAddedCombos;
    @BindView(R.id.layout_combos_container)
    View layoutComboContainer;
    @BindView(R.id.btn_add_combo)
    Button btnAddCombo;
    ArrayList<String> serviceNames = new ArrayList<>();
    ArrayList<String> mrps = new ArrayList<>();
    ArrayList<String> prices = new ArrayList<>();
    AddComboPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    CategoriesListDialog categoriesListDialog;
    Category[] categories;
    int categoryId = 0;
    ArrayList<AddedCombo> addedCombos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_combo);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Add Combo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAddComboDialog();
        setUpProgressDialog();
        initialisePresenter();
        setCategoriesListDialog();
        mPresenter.fetchCategories();
    }

    private void initialisePresenter() {
        mPresenter = new AddComboPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    private void setCategoriesListDialog() {
        categoriesListDialog = new CategoriesListDialog(this, this, this);
        categoriesListDialog.setUpDialogView();
    }

    private void setAddComboDialog() {
        View view  = LayoutInflater.from(this).inflate(R.layout.layout_add_combo_service_bottomsheets, null);
        addComboDialog = new BottomSheetDialog(this);
        addComboDialog.setContentView(view);
        editTextPrice = (EditText) view.findViewById(R.id.edit_text_price);
        editTextMrp = (EditText) view.findViewById(R.id.edit_text_mrp);
        editTextServiceName = (EditText) view.findViewById(R.id.edit_text_service_name);
        Button btnSubmit = (Button) view.findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextServiceName.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Service Name", Toast.LENGTH_SHORT).show();
                } else if (editTextMrp.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Mrp", Toast.LENGTH_SHORT).show();
                } else if (editTextPrice.getText().toString().trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter Price", Toast.LENGTH_SHORT).show();
                } else {
                    addCombos(editTextServiceName.getText().toString(), editTextMrp.getText().toString(), editTextPrice.getText().toString());
                }
            }
        });

    }

    @OnClick(R.id.btn_add_combo) void onAddComboClicked() {
        addComboDialog.show();
    }

    @OnClick(R.id.txt_view_select_category) void onEditTextCategoryList() {
        categoriesListDialog.showDialog();
    }

    @Override
    public void loadCategories(Category[] categories) {
        this.categories = categories;
        categoriesListDialog.setCategories(categories);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onAddComboSuccess() {
        finish();
    }

    @Override
    public void onCategorySelected(int position) {
        categoryId = categories[position].id;
        txtViewSelectCategory.setText(categories[position].name);
        categoriesListDialog.hideDialog();
    }

    private void addCombos(String comboName, String mrp, String price) {
        addedCombos.add(new AddedCombo(comboName, mrp, price));
        AddedCombosAdapter addedCombosAdapter = new AddedCombosAdapter(this, addedCombos);
        recyclerViewAddedCombos.setAdapter(addedCombosAdapter);
        recyclerViewAddedCombos.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAddedCombos.setVisibility(View.VISIBLE);
        layoutComboContainer.setVisibility(View.VISIBLE);
        btnAddCombo.setVisibility(View.GONE);
        addComboDialog.dismiss();
    }

    @OnClick(R.id.btn_add_more) void onAddMoreClicked() {
        editTextServiceName.setText("");
        editTextMrp.setText("");
        editTextPrice.setText("");
        addComboDialog.show();
    }

    @OnClick(R.id.btn_confirm) void onConfirmClicked() {
        if (categoryId == 0) {
            Toast.makeText(this, "Please select a category", Toast.LENGTH_SHORT).show();
        } else if (editTextComboName.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter a combo name", Toast.LENGTH_SHORT).show();
        } else {
            for (int i = 0; i < addedCombos.size(); i++) {
                serviceNames.add(addedCombos.get(i).comboName);
                mrps.add(addedCombos.get(i).mrp);
                prices.add(addedCombos.get(i).price);
            }
            mPresenter.addCombo(editTextComboName.getText().toString(), categoryId, serviceNames, mrps, prices);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}