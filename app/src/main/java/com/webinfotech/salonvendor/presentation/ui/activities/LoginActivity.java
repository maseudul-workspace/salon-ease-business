package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.LoginPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.LoginPresenterImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.edit_text_phone_no)
    EditText editTextPhoneNo;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_view_password_show)
    TextView txtViewPasswordShow;
    @BindView(R.id.txt_view_password_hide)
    TextView txtViewPasswordHide;
    ProgressDialog progressDialog;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<b>Log In</b>"));
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked() {
        Intent intent = new Intent(this, ForgetPasswordOtpActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_register) void onRegisterClicked() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {

        if (editTextPhoneNo.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_LONG).show();
        } else if (editTextPhoneNo.getText().toString().trim().length() < 10) {
            Toast.makeText(this, "Please enter a 10 digit phone no", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.checkLogin(editTextPhoneNo.getText().toString(), editTextPassword.getText().toString());
        }

    }
        @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.txt_view_password_show) void onPasswordShowClicked() {
        editTextPassword.setTransformationMethod(null);
        txtViewPasswordShow.setVisibility(View.GONE);
        txtViewPasswordHide.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_view_password_hide) void onPasswordHideClicked() {
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
        txtViewPasswordShow.setVisibility(View.VISIBLE);
        txtViewPasswordHide.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}