package com.webinfotech.salonvendor.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.webinfotech.salonvendor.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditPriceDialog {

    public interface Callback {
        void editPrice(String mrp, String price);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_price)
    EditText editTextPrice;
    @BindView(R.id.edit_text_mrp)
    EditText editTextMrp;

    public EditPriceDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_edit_price_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setPrice(String mrp, String price) {
        editTextMrp.setText(mrp);
        editTextPrice.setText(price);
    }

    @OnClick(R.id.btn_change_price) void onChangepriceClicked() {
        if (editTextPrice.getText().toString().trim().isEmpty() || editTextMrp.getText().toString().trim().isEmpty()) {
            Toast.makeText(mContext, "Please Fill All The Fields", Toast.LENGTH_SHORT).show();
        } else {
            mCallback.editPrice(editTextMrp.getText().toString(), editTextPrice.getText().toString());
            hideDialog();
        }
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }

}
