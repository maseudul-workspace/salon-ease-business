package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.SendOtpForgetPasswordInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.SendOtpForgetPasswordInteractorImpl;
import com.webinfotech.salonvendor.presentation.presenters.ForgetPasswordOtpVerifyPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class ForgetPasswordOtpVerifyPresenterImpl extends AbstractPresenter implements ForgetPasswordOtpVerifyPresenter, SendOtpForgetPasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordOtpVerifyPresenter.View mView;

    public ForgetPasswordOtpVerifyPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendOtpForgetPasswordInteractorImpl sendOtpForgetPasswordInteractor = new SendOtpForgetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        sendOtpForgetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOtpSendSuccess(String otp) {
        mView.hideLoader();
        mView.onOtpSendSuccess(otp);
    }

    @Override
    public void onOtpSendFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
