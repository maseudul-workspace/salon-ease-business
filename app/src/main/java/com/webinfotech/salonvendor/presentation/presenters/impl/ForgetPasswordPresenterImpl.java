package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.RequestPasswordChangeInteractor;
import com.webinfotech.salonvendor.domain.interactors.ResetPasswordInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.RequestPasswordChangeInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.ResetPasswordInteractorImpl;
import com.webinfotech.salonvendor.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ResetPasswordInteractor.Callback, RequestPasswordChangeInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void requestPasswordChange(String mobile) {
        RequestPasswordChangeInteractorImpl requestPasswordChangeInteractor = new RequestPasswordChangeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile);
        requestPasswordChangeInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void resetPassword(String phone, String otp, String password) {
        ResetPasswordInteractorImpl resetPasswordInteractor = new ResetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, password, otp);
        resetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onRequestPasswordChangedSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Request sent to admin", Toast.LENGTH_LONG).show();
        mView.onPasswordRequestSuccess();
    }

    @Override
    public void onRequestPasswordChangedFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPasswordResetSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Password Changed Successfully", Toast.LENGTH_LONG).show();
        mView.onPasswordRequestSuccess();
    }

    @Override
    public void onPasswordResetFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
