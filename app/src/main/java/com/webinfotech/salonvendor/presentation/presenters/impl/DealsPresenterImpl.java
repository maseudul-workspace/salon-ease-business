package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.AddDealInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchDealsInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchServicesListInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.salonvendor.domain.interactors.RemoveDealInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.AddDealInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchDealsInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchServicesListInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchThirdCategoryInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.RemoveDealInteractorImpl;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.DealsPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class DealsPresenterImpl extends AbstractPresenter implements    DealsPresenter,
                                                                        FetchDealsInteractor.Callback,
                                                                        FetchServicesListInteractor.Callback,
                                                                        AddDealInteractor.Callback,
                                                                        RemoveDealInteractor.Callback
{

    Context mContext;
    DealsPresenter.View mView;

    public DealsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchDeals() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchDealsInteractorImpl fetchDealsInteractor = new FetchDealsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchDealsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchServices() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            FetchServicesListInteractorImpl fetchServicesListInteractor = new FetchServicesListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, userDetails.id);
            fetchServicesListInteractor.execute();
        }
    }

    @Override
    public void addDeal(int clientServiceId, String discount, String expiryDate) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            AddDealInteractorImpl addDealInteractor = new AddDealInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, clientServiceId, discount, expiryDate);
            addDealInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void removeDeal(int clientServiceId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserDetails userDetails = androidApplication.getUserInfo(mContext);
        if (userDetails != null) {
            RemoveDealInteractorImpl removeDealInteractor = new RemoveDealInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userDetails.api_token, clientServiceId);
            removeDealInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onDealAddSuccess() {
        fetchDeals();
    }

    @Override
    public void onDealAddFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingServicesListSuccess(Service[] services) {
        mView.loadServices(services);
    }

    @Override
    public void onGettingServicesListFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }

    @Override
    public void onGettingDealsListSuccess(Service[] services) {
        mView.loadDeals(services);
        mView.hideLoader();
    }

    @Override
    public void onGettingDealsListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onDealRemoveSuccess() {
        fetchDeals();
    }

    @Override
    public void onDealRemoveFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

}
