package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Orders;
import com.webinfotech.salonvendor.util.GlideHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    public interface Callback {
        void changeOrderStatus(int orderId, int status);
        void onLocationClicked(double latitude, double longitude);
    }

    Context mContext;
    Orders[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Orders[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_orders, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.txtViewOrderNo.setText("ORDER NO " + orders[position].id);
            holder.txtViewOrderDate.setText("Service Date: " + changeDataFormat(orders[position].serviceTime));
            holder.txtViewPayableAmount.setText("Rs. " + (orders[position].amount - orders[position].advanceAmount));
            holder.txtViewAdvanceAmount.setText("Rs. " + orders[position].advanceAmount);

            switch (orders[position].orderStatus) {
                case 1:
                    holder.txtViewOrderStatus.setText("Processing");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
                    if (orders[position].paymentStatus == 1) {
                        holder.btnReject.setVisibility(View.GONE);
                        holder.btnAccept.setVisibility(View.GONE);
                    } else {
                        holder.btnReject.setVisibility(View.VISIBLE);
                        holder.btnAccept.setVisibility(View.VISIBLE);
                    }
                    holder.btnComplete.setVisibility(View.GONE);
                    break;
                case 2:
                    holder.txtViewOrderStatus.setText("Accepted");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
                    holder.btnReject.setVisibility(View.GONE);
                    holder.btnAccept.setVisibility(View.GONE);
                    holder.btnComplete.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    holder.txtViewOrderStatus.setText("Rescheduled");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
                    if (orders[position].paymentStatus == 1) {
                        holder.btnReject.setVisibility(View.GONE);
                        holder.btnAccept.setVisibility(View.GONE);
                    } else {
                        holder.btnReject.setVisibility(View.VISIBLE);
                        holder.btnAccept.setVisibility(View.VISIBLE);
                    }
                    holder.btnComplete.setVisibility(View.GONE);
                    break;
                case 4:
                    holder.txtViewOrderStatus.setText("Completed");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                    holder.btnReject.setVisibility(View.GONE);
                    holder.btnAccept.setVisibility(View.GONE);
                    holder.btnComplete.setVisibility(View.GONE);
                    break;
                case 5:
                    holder.txtViewOrderStatus.setText("Cancelled");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_400));
                    holder.btnReject.setVisibility(View.GONE);
                    holder.btnAccept.setVisibility(View.GONE);
                    holder.btnComplete.setVisibility(View.GONE);
                    break;
            }

            if (orders[position].paymentStatus == 1) {
                holder.txtViewPaymentStatus.setText("Failed");
            } else {
                holder.txtViewPaymentStatus.setText("Paid");
            }

            holder.btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("Do you want to cancel this booking ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='black'><b>Yes</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.changeOrderStatus(orders[position].id, 5);
                        }
                    });

                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });

            holder.btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("Do you want to accept this booking ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='black'><b>Yes</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.changeOrderStatus(orders[position].id, 2);
                        }
                    });

                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });

            holder.btnComplete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("Do you want to complete this booking ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='black'><b>Yes</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.changeOrderStatus(orders[position].id, 4);
                        }
                    });

                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });

        } catch (Exception e) {

        }
        try {
            holder.txtViewAddres.setText(orders[position].userProfile.address + ", " + orders[position].userProfile.city + ", " + orders[position].userProfile.state + ", " + orders[position].userProfile.pin);
            holder.txtViewPhone.setText(orders[position].userProfile.mobile);
            holder.txtViewSalonName.setText("Order By " + orders[position].userProfile.name);
        } catch (NullPointerException e) {

        }
        holder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onLocationClicked(orders[position].userProfile.latitude, orders[position].userProfile.longitude);
            }
        });

        OrderServicesAdapter adapter = new OrderServicesAdapter(mContext, orders[position].serviceOrders);
        holder.recyclerViewOrderServices.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerViewOrderServices.setAdapter(adapter);
        holder.recyclerViewOrderServices.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));

        holder.layoutServicesHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.recyclerViewOrderServices.getVisibility() == View.VISIBLE) {
                    holder.recyclerViewOrderServices.setVisibility(View.GONE);
                } else {
                    holder.recyclerViewOrderServices.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_order_no)
        TextView txtViewOrderNo;
        @BindView(R.id.txt_view_payable_amount)
        TextView txtViewPayableAmount;
        @BindView(R.id.txt_view_advance_amount)
        TextView txtViewAdvanceAmount;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.txt_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.btn_reject)
        Button btnReject;
        @BindView(R.id.btn_accept)
        Button btnAccept;
        @BindView(R.id.btn_complete)
        Button btnComplete;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddres;
        @BindView(R.id.btn_location)
        Button btnLocation;
        @BindView(R.id.txt_view_phone)
        TextView txtViewPhone;
        @BindView(R.id.recycler_view_order_services)
        RecyclerView recyclerViewOrderServices;
        @BindView(R.id.layout_services_header)
        View layoutServicesHeader;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String changeDataFormat(String dateString) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "EEEE, dd MMM, yyyy hh:mm aa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateString);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
