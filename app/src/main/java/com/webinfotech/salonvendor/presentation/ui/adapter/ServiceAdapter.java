package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.util.GlideHelper;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(Service service);
        void updateServiceStatus(int serviceId, int status);
    }

    Context mContext;
    Service[] services;
    Callback mCallback;

    public ServiceAdapter(Context mContext, Service[] services, Callback mCallback) {
        this.mContext = mContext;
        this.services = services;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewChangePrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(services[position]);
            }
        });
        
        if (services[position].status == 1) {
            holder.switchAvailability.setChecked(true);
            holder.switchAvailability.setText("Available");
        } else {
            holder.switchAvailability.setChecked(false);
            holder.switchAvailability.setText("Not Available");
        }

        holder.switchAvailability.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.updateServiceStatus(services[position].id, 1);
                } else {
                    mCallback.updateServiceStatus(services[position].id, 2);
                }
            }
        });
        holder.txtViewMainCategory.setText(services[position].mainCategory + " -> " + services[position].subCategory);
        holder.txtViewSubcategory.setText(services[position].lastCategory);
        holder.txtViewMrp.setText("Rs. " + services[position].mrp);
        holder.txtViewPrice.setText("Rs. " + services[position].price);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewMainCategoryIcon, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + services[position].lastCategoryImage, 100);
    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_category)
        ImageView imgViewMainCategoryIcon;
        @BindView(R.id.txt_view_main_category)
        TextView txtViewMainCategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_change_price)
        TextView txtViewChangePrice;
        @BindView(R.id.switch_availability)
        SwitchMaterial switchAvailability;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
