package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.domain.models.UserProfile;

public interface CompleteProfilePresenter {
    void fetchUserProfile();
    void fetchCities();
    void updateProfile(String name, String mobile, String workExperience, String state, String city, int serviceCityId, String address, String pin, String email, String gst, double latitude, double longitude, String openingTime, String closing_time, String description, String profileImageFilePath, boolean isNewImage, String addressProofFile, String addressProof, String photoProofFile, String photoProof, String businessProofFile, String businessProof, int ac, int parking, int wifi, int music, boolean isNewIdProof, boolean isNewBusinessProof, boolean isNewAddressProof);
    interface View {
        void loadUserProfile(UserProfile userProfile);
        void loadCities(City[] cities);
        void showLoader();
        void hideLoader();
        void onProfileUpdateSuccess();
    }
}
