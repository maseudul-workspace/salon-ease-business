package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;
import com.webinfotech.salonvendor.util.GlideHelper;

import java.security.PublicKey;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ThirdCategoryAdapter extends RecyclerView.Adapter<ThirdCategoryAdapter.ViewHolder> {

    public interface Callback {
        void onThirdCategoryClicked(int position);
    }

    Context mContext;
    ThirdCategory[] thirdCategories;
    Callback mCallback;

    public ThirdCategoryAdapter(Context mContext, ThirdCategory[] thirdCategories, Callback mCallback) {
        this.mContext = mContext;
        this.thirdCategories = thirdCategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_categories, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCategory.setText(thirdCategories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewCategory, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + thirdCategories[position].image, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onThirdCategoryClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return thirdCategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategory;
        @BindView(R.id.img_view_category)
        ImageView imgViewCategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
