package com.webinfotech.salonvendor.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.webinfotech.salonvendor.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TncDialog {

    public interface Callback {
        void onAgreeClicked();
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.checkbox_tnc)
    CheckBox checkBoxTnc;
    Callback mCallback;

    public TncDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_tnc_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_agree) void onAgreeClicked() {
        if (checkBoxTnc.isChecked()) {
            mCallback.onAgreeClicked();
            hideDialog();
        } else {
            YoYo.with(Techniques.Shake).duration(1200).playOn(checkBoxTnc);
        }
    }

}
