package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonvendor.domain.interactors.RegisterUserInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonvendor.domain.interactors.impl.RegisterUserInteractorImpl;
import com.webinfotech.salonvendor.domain.models.City;
import com.webinfotech.salonvendor.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;

public class RegisterUserPresenterImpl extends AbstractPresenter implements RegisterUserPresenter,
                                                                            RegisterUserInteractor.Callback,
                                                                            FetchCityListInteractor.Callback
{

    Context mContext;
    RegisterUserPresenter.View mView;

    public RegisterUserPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void registerUser(String name, String mobile, String password, String otp, int clientType, int serviceCityId, double latitude, double longitude) {
        RegisterUserInteractorImpl registerUserInteractor = new RegisterUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, name, mobile, password, otp, clientType, serviceCityId, latitude, longitude);
        registerUserInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchCityList() {
        FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchCityListInteractor.execute();
    }

    @Override
    public void onRegisterUserSuccess() {
        mView.hideLoader();
        mView.onUserRegistrationSuccess();
        Toast.makeText(mContext, "User Created Successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRegisterUserFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {

    }
}
