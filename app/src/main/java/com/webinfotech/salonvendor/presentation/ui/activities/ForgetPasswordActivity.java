package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class ForgetPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View {

    @BindView(R.id.edit_text_pswd)
    EditText editTextPswd;
    @BindView(R.id.edit_text_repeat_pswd)
    EditText editTextRepeatPswd;
    ForgetPasswordPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String phoneNo;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        getSupportActionBar().setTitle("Request Password Change");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        phoneNo = getIntent().getStringExtra("phoneNo");
        otp = getIntent().getStringExtra("otp");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if ( editTextPswd.getText().toString().trim().isEmpty()) {
            Toast.makeText(this,"Please Insert Your Password", Toast.LENGTH_LONG).show();
        } else if (editTextRepeatPswd.getText().toString().trim().isEmpty()) {
            Toast.makeText(this,"Please Repeat Your Password", Toast.LENGTH_LONG).show();
        } else if (!editTextRepeatPswd.getText().toString().trim().equals(editTextPswd.getText().toString().trim())) {
            Toast.makeText(this,"Password Mismatched", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.resetPassword(phoneNo, otp, editTextPswd.getText().toString());
        }
    }

    @Override
    public void onPasswordRequestSuccess() {
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}