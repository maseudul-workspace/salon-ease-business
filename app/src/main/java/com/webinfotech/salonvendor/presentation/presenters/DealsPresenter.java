package com.webinfotech.salonvendor.presentation.presenters;

import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;

public interface DealsPresenter {
    void fetchDeals();
    void fetchServices();
    void addDeal(int clientServiceId, String discount, String expiryDate);
    void removeDeal(int clientServiceId);
    interface View {
        void loadServices(Service[] services);
        void loadDeals(Service[] services);
        void showLoader();
        void hideLoader();
    }

}
