package com.webinfotech.salonvendor.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.executors.Executor;
import com.webinfotech.salonvendor.domain.executors.MainThread;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.interactors.CheckLoginInteractor;
import com.webinfotech.salonvendor.domain.interactors.impl.CheckLoginInteractorImpl;
import com.webinfotech.salonvendor.domain.models.UserDetails;
import com.webinfotech.salonvendor.presentation.presenters.LoginPresenter;
import com.webinfotech.salonvendor.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonvendor.repository.AppRepositoryImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter,
                                                                    CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String phone, String password) {
        CheckLoginInteractorImpl checkLoginInteractor = new CheckLoginInteractorImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), new AppRepositoryImpl(), this, phone, password);
        checkLoginInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onLoginSuccess(UserDetails userDetails) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userDetails);
//        Log.e("LogMsg", "Api Token: " + userDetails.api_token);
//        Log.e("LogMsg", "Api Token: " + userDetails.id);
        mView.hideLoader();
        mView.onLoginSuccess();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

}
