package com.webinfotech.salonvendor.presentation.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.models.Images;
import com.webinfotech.salonvendor.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ViewHolder> {

    public interface Callback {
        void onImageClicked(int id);
        void onDeleteClicked(int id, int position);
    }

    Context mContext;
    Images[] images;
    Callback mCallback;

    public ImageGalleryAdapter(Context mContext, Images[] images, Callback mCallback) {
        this.mContext = mContext;
        this.images = images;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_image_gallery, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewGallery, mContext.getResources().getString(R.string.base_url) + "images/client/" + images[position].image, 10);
        holder.imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation dialog");
                builder.setMessage("You are about to remove an image. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'><b>Yes</b></font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(images[position].id, position);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_gallery)
        ImageView imgViewGallery;
        @BindView(R.id.img_view_cancel)
        ImageView imgViewCancel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
