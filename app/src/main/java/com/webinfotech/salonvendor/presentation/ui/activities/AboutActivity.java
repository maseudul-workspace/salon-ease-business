package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.webinfotech.salonvendor.R;

public class AboutActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_about)
    TextView txtViewAbout;
    @BindView(R.id.layout_refund_policy)
    View layoutRefundPolicy;
    @BindView(R.id.txt_view_disclaimer)
    TextView txtViewDisclaimer;
    @BindView(R.id.layout_privacy_policy)
    View layoutPrivacyPolicy;
    @BindView(R.id.layout_terms_condition)
    View layoutTermsCondition;
    @BindView(R.id.layout_faq)
    View layoutFaq;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("About Us");
        setBottomNavigationView();
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setSelectedItemId(R.id.nav_about);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_profile:
                        Intent intent = new Intent(getApplicationContext(), UserActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_bookings:
                        Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainIntent);
                        break;
                    case R.id.nav_notification:
                        Intent notificationIntent = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(notificationIntent);
                        break;
                    case R.id.nav_contact_us:
                        Intent contactusIntent = new Intent(getApplicationContext(), ContactusActivity.class);
                        startActivity(contactusIntent);
                        break;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.layout_about_us_header) void onAboutUsClicked() {
        if (txtViewAbout.getVisibility() == View.VISIBLE) {
            txtViewAbout.setVisibility(View.GONE);
        } else {
            txtViewAbout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_refund_cancellation_policy_header) void onLayoutRefundClicked() {
        if (layoutRefundPolicy.getVisibility() == View.VISIBLE) {
            layoutRefundPolicy.setVisibility(View.GONE);
        } else {
            layoutRefundPolicy.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_disclaimer_header) void onDisclaimerClicked() {
        if (txtViewDisclaimer.getVisibility() == View.VISIBLE) {
            txtViewDisclaimer.setVisibility(View.GONE);
        } else {
            txtViewDisclaimer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_privacy_policy_header) void onPrivacyPolicyClicked() {
        if (layoutPrivacyPolicy.getVisibility() == View.VISIBLE){
            layoutPrivacyPolicy.setVisibility(View.GONE);
        } else {
            layoutPrivacyPolicy.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_terms_condition_header) void onermsConsitionClicked() {
        if (layoutTermsCondition.getVisibility() == View.VISIBLE) {
            layoutTermsCondition.setVisibility(View.GONE);
        } else {
            layoutTermsCondition.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_faq_header) void onLayoutFaqHeaderClicked() {
        if (layoutFaq.getVisibility() == View.VISIBLE) {
            layoutFaq.setVisibility(View.GONE);
        } else {
            layoutFaq.setVisibility(View.VISIBLE);
        }
    }

}