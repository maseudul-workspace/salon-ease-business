package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.domain.models.Category;
import com.webinfotech.salonvendor.domain.models.Service;
import com.webinfotech.salonvendor.domain.models.Subcategory;
import com.webinfotech.salonvendor.domain.models.ThirdCategory;
import com.webinfotech.salonvendor.presentation.presenters.DealsPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.DealsPresenterImpl;
import com.webinfotech.salonvendor.presentation.ui.adapter.DealsListAdapter;
import com.webinfotech.salonvendor.presentation.ui.adapter.ServiceDialogListAdapter;
import com.webinfotech.salonvendor.presentation.ui.dialogs.CategoriesListDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.ServicesListDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.SubcategoriesListDialog;
import com.webinfotech.salonvendor.presentation.ui.dialogs.ThirdCategoryListDialog;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MyDealsActivity extends AppCompatActivity implements DealsPresenter.View, ServiceDialogListAdapter.Callback, DealsListAdapter.Callback {

    BottomSheetDialog addDealDialog;
    ServicesListDialog servicesListDialog;
    DealsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int clientServiceId = 0;
    EditText editTextServiceList;
    @BindView(R.id.recycler_view_deals)
    RecyclerView recyclerViewDeals;
    EditText editTextDiscount;
    EditText editTextExpiryDate;
    Service[] deals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_deals);
        getSupportActionBar().setTitle("My Deals");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        setBottomSheetDialog();
        setDialogs();
        initialisePresenter();
        mPresenter.fetchServices();
        mPresenter.fetchDeals();
    }

    private void initialisePresenter() {
        mPresenter = new DealsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setDialogs() {
       servicesListDialog = new ServicesListDialog(this, this);
       servicesListDialog.setUpDialogView();
    }

    private void setBottomSheetDialog() {
        if (addDealDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_add_deal_bottom_sheet, null);
            addDealDialog = new BottomSheetDialog(this);
            editTextServiceList = (EditText) view.findViewById(R.id.edit_text_service_list);
            editTextDiscount = (EditText) view.findViewById(R.id.edit_text_discount_percentage);
            editTextExpiryDate = (EditText) view.findViewById(R.id.edit_text_expiry_date);
            Button btnSubnit = (Button) view.findViewById(R.id.btn_submit);

            editTextServiceList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    servicesListDialog.showDialog();
                }
            });

            editTextExpiryDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(MyDealsActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    String expiryDate = parseDateToddMMyyyy(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    editTextExpiryDate.setText(expiryDate);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            });

            btnSubnit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clientServiceId == 0) {
                        Toast.makeText(getApplicationContext(), "Please choose your service", Toast.LENGTH_SHORT).show();
                    } else if (editTextDiscount.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter discount", Toast.LENGTH_SHORT).show();
                    } else if (editTextExpiryDate.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter expiry date", Toast.LENGTH_SHORT).show();
                    } else {
                        mPresenter.addDeal(clientServiceId, editTextDiscount.getText().toString(), editTextExpiryDate.getText().toString());
                        addDealDialog.dismiss();
                    }
                }
            });

            addDealDialog.setContentView(view);
        }
    }

    @OnClick(R.id.btn_add_deal) void onAddDealClicked() {
        addDealDialog.show();
    }

    @Override
    public void loadServices(Service[] services) {
        ServiceDialogListAdapter adapter = new ServiceDialogListAdapter(this, services, this);
        servicesListDialog.setData(adapter);
    }

    @Override
    public void loadDeals(Service[] services) {
        deals = services;
        DealsListAdapter adapter = new DealsListAdapter(this, services, this);
        recyclerViewDeals.setAdapter(adapter);
        recyclerViewDeals.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "yyyy-MM-dd";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void onServiceClicked(int clientServiceId, String serviceName) {
        servicesListDialog.hideDialog();
        this.clientServiceId = clientServiceId;
        editTextServiceList.setText(serviceName);
    }

    @Override
    public void onEditClicked(int position) {
        clientServiceId = deals[position].id;
        editTextServiceList.setText(deals[position].lastCategory);
        editTextExpiryDate.setText(deals[position].expiryDate);
        editTextDiscount.setText(Double.toString(deals[position].discount));
        addDealDialog.show();
    }

    @Override
    public void onRemoveClicked(int serviceId) {
        mPresenter.removeDeal(serviceId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}