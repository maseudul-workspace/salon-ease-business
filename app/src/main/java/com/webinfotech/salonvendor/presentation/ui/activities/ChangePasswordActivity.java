package com.webinfotech.salonvendor.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.webinfotech.salonvendor.R;
import com.webinfotech.salonvendor.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonvendor.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.salonvendor.presentation.presenters.impl.ChangePasswordPresenterImpl;
import com.webinfotech.salonvendor.threading.MainThreadImpl;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.edit_text_current_password)
    EditText editTextCurrentPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    ProgressDialog progressDialog;
    ChangePasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
       mPresenter = new ChangePasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextNewPassword.getText().toString().trim().isEmpty() || editTextCurrentPassword.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "All Fields Required", Toast.LENGTH_SHORT).show();
        } else if (editTextNewPassword.getText().toString().trim().length() < 8 || editTextCurrentPassword.getText().toString().trim().length() < 8) {
            Toast.makeText(this, "Passwords must be at least 8 characters", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.changePassword(editTextCurrentPassword.getText().toString(), editTextNewPassword.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onPasswordChangedSuccess() {
        editTextCurrentPassword.setText("");
        editTextNewPassword.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}