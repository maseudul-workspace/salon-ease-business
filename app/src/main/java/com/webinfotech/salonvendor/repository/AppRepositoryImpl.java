package com.webinfotech.salonvendor.repository;

import android.util.Log;

import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.gson.Gson;
import com.webinfotech.salonvendor.domain.models.CategoryWrapper;
import com.webinfotech.salonvendor.domain.models.CityWrapper;
import com.webinfotech.salonvendor.domain.models.ComboDataWrapper;
import com.webinfotech.salonvendor.domain.models.CommonResponse;
import com.webinfotech.salonvendor.domain.models.MessageWrapper;
import com.webinfotech.salonvendor.domain.models.OrdersWrapper;
import com.webinfotech.salonvendor.domain.models.OtpResponse;
import com.webinfotech.salonvendor.domain.models.RegistrationResponse;
import com.webinfotech.salonvendor.domain.models.ServicesWrapper;
import com.webinfotech.salonvendor.domain.models.SubcategoryWrapper;
import com.webinfotech.salonvendor.domain.models.ThirdCategoryWrapper;
import com.webinfotech.salonvendor.domain.models.UserDetailsWrapper;
import com.webinfotech.salonvendor.domain.models.UserProfileWrapper;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public OtpResponse sendOtp(String mobile, int userType) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(mobile, userType);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public RegistrationResponse registerUser(String name,
                                             String mobile,
                                             String password,
                                             String otp,
                                             int clientType,
                                             int serviceCityId,
                                             double latitude,
                                             double longitude
    ) {
        RegistrationResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerUser(name, mobile, password, otp, clientType, serviceCityId, latitude, longitude);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, RegistrationResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CityWrapper fetchCityList() {
        CityWrapper cityWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCityList();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cityWrapper = null;
                }else{
                    cityWrapper = gson.fromJson(responseBody, CityWrapper.class);
                }
            } else {
                cityWrapper = null;
            }
        }catch (Exception e){
            cityWrapper = null;
        }
        return cityWrapper;
    }

    public UserDetailsWrapper checkLogin(String userName, String password) {
        UserDetailsWrapper userDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(userName, password);
            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userDetailsWrapper = null;
                }else{
                    userDetailsWrapper = gson.fromJson(responseBody, UserDetailsWrapper.class);
                }
            } else {
                userDetailsWrapper = null;
            }
        }catch (Exception e){
            userDetailsWrapper = null;
        }
        return userDetailsWrapper;
    }

    public CommonResponse uploadImage(String apiToken,
                                      String filePath,
                                      int clientId) {

        CommonResponse commonResponse;

        RequestBody clientIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(clientId));

        File imageFile = new File(filePath);

        // create RequestBody instance from file
        RequestBody requestImageFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        imageFile
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part imageFileBody =
                MultipartBody.Part.createFormData("images[0]", imageFile.getName(), requestImageFile);

        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addImages("Bearer " + apiToken, clientIdRequestBody, imageFileBody);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }

        return commonResponse;
    }

    public UserProfileWrapper fetchUserProfile(String apiToken, int userId) {
        UserProfileWrapper userProfileWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userProfileWrapper = null;
                }else{
                    userProfileWrapper = gson.fromJson(responseBody, UserProfileWrapper.class);
                }
            } else {
                userProfileWrapper = null;
            }
        }catch (Exception e){
            userProfileWrapper = null;
        }
        return userProfileWrapper;
    }

    public CommonResponse deleteGalleryImage(String apiToken, int userId, int imageId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteGalleryImage("Bearer " + apiToken, userId, imageId);
            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateProfile(String authorization,
                                      int clientId,
                                      String name,
                                      String mobile,
                                      String workExperience,
                                      String state,
                                      String city,
                                      int serviceCityId,
                                      String address,
                                      String pin,
                                      String email,
                                      String gst,
                                      double latitude,
                                      double longitude,
                                      String openingTime,
                                      String closing_time,
                                      String description,
                                      String profileImageFilePath,
                                      boolean isNewImage,
                                        String addressProofFile,
                                        String addressProof,
                                        String photoProofFile,
                                        String photoProof,
                                        String businessProofFile,
                                        String businessProof,
                                        int ac,
                                        int parking,
                                        int wifi,
                                        int music,
                                        boolean isNewIdProof,
                                        boolean isNewBusinessProof,
                                        boolean isNewAddressProof
                                      ) {


        CommonResponse commonResponse = null;
        try {

            RequestBody clientIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(clientId));
            RequestBody nameRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, name);
            RequestBody mobileRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, mobile);
            RequestBody workExperienceRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, workExperience);
            RequestBody stateRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, state);
            RequestBody cityRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, city);
            RequestBody serviceCityIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(serviceCityId));
            RequestBody addressRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, address);
            RequestBody pinRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, pin);
            RequestBody emailRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, email);
            RequestBody gstRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, gst);
            RequestBody latitudeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Double.toString(latitude));
            RequestBody longitudeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Double.toString(longitude));
            RequestBody openingTimeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, openingTime);
            RequestBody closingTimeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, closing_time);
            RequestBody descriptionRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, description);
            RequestBody addressProofRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, addressProof);
            RequestBody photoProofRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, photoProof);
            RequestBody businessProofRequestBody;
            try {
                businessProofRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, businessProof);

            } catch (Exception e) {
                businessProofRequestBody = null;
            }
            RequestBody acRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(ac));
            RequestBody parkingRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(parking));
            RequestBody wifiRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(wifi));
            RequestBody musicRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(music));

            MultipartBody.Part imageFileBody = null;
            if (isNewImage && profileImageFilePath != null) {
                File imageFile = new File(profileImageFilePath);
                // create RequestBody instance from file
                RequestBody requestImageFile =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                imageFile
                        );

                // MultipartBody.Part is used to send also the actual file name
                imageFileBody = MultipartBody.Part.createFormData("profile_image", imageFile.getName(), requestImageFile);
            }

            MultipartBody.Part addressProofFileBody = null;
            if (isNewAddressProof && addressProofFile != null) {
                File file = new File(addressProofFile);
                // create RequestBody instance from file
                RequestBody requestImageFile =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                addressProofFileBody = MultipartBody.Part.createFormData("address_proof_file", file.getName(), requestImageFile);
            }
            MultipartBody.Part photoProofFileBody = null;
            if (isNewIdProof && photoProofFile != null) {
                File file = new File(photoProofFile);
                // create RequestBody instance from file
                RequestBody requestImageFile =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                photoProofFileBody = MultipartBody.Part.createFormData("photo_proof_file", file.getName(), requestImageFile);
            }

            MultipartBody.Part businessProofFileBody = null;
            if (isNewBusinessProof && businessProofFile != null) {
                File file = new File(businessProofFile);
                // create RequestBody instance from file
                RequestBody requestImageFile =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                businessProofFileBody = MultipartBody.Part.createFormData("business_proof_file", file.getName(), requestImageFile);
            }


            String responseBody = "";
            Gson gson = new Gson();
            Boolean isErrorResponse = false;
            try {
                Call<ResponseBody> add = mRepository.updateClient("Bearer " + authorization,
                        clientIdRequestBody,
                        nameRequestBody,
                        mobileRequestBody,
                        workExperienceRequestBody,
                        stateRequestBody,
                        cityRequestBody,
                        serviceCityIdRequestBody,
                        addressRequestBody,
                        pinRequestBody,
                        emailRequestBody,
                        gstRequestBody,
                        latitudeRequestBody,
                        longitudeRequestBody,
                        openingTimeRequestBody,
                        closingTimeRequestBody,
                        descriptionRequestBody,
                        imageFileBody,
                        addressProofFileBody,
                        addressProofRequestBody,
                        photoProofFileBody,
                        photoProofRequestBody,
                        businessProofFileBody,
                        businessProofRequestBody,
                        acRequestBody,
                        parkingRequestBody,
                        wifiRequestBody,
                        musicRequestBody
                );
                Response<ResponseBody> response = add.execute();
                if(response.body() != null){
                    responseBody = response.body().string();
                } else if(response.errorBody() != null){
                    responseBody = response.errorBody().string();
                }
                if (!responseBody.isEmpty()) {
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            }catch (Exception e){
                commonResponse = null;
            }
        } catch (Exception e) {
        }

        return commonResponse;
    }

    public CommonResponse updateSchedule(String apiToken, int clientId, String date, int status) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> update = mRepository.updateSchedule("Bearer " + apiToken, clientId, date, status);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                commonResponse = gson.fromJson(responseBody, CommonResponse.class);
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CategoryWrapper fetchCategories() {
        CategoryWrapper categoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCategories();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryWrapper = null;
                }else{
                    categoryWrapper = gson.fromJson(responseBody, CategoryWrapper.class);
                }
            } else {
                categoryWrapper = null;
            }
        }catch (Exception e){
            categoryWrapper = null;
        }
        return categoryWrapper;
    }

    public SubcategoryWrapper fetchSubcategory(int categoryId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSecondCategory(categoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
        }
        return subcategoryWrapper;
    }

    public ThirdCategoryWrapper fetchThirdCategoryy(int subcategoryId) {
        ThirdCategoryWrapper thirdCategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchThirdCategory(subcategoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    thirdCategoryWrapper = null;
                }else{
                    thirdCategoryWrapper = gson.fromJson(responseBody, ThirdCategoryWrapper.class);
                }
            } else {
                thirdCategoryWrapper = null;
            }
        }catch (Exception e){
            thirdCategoryWrapper = null;
        }
        return thirdCategoryWrapper;
    }

    public CommonResponse addService(String authorization,
                                     int clientId,
                                     ArrayList<Integer> mainCategoryIds,
                                     ArrayList<Integer> subCategoryIds,
                                     ArrayList<Integer> lastCategoryIds,
                                     ArrayList<String> mrp,
                                     ArrayList<String> price) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addService("Bearer " + authorization, clientId, mainCategoryIds, subCategoryIds, lastCategoryIds, mrp, price);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateService(String authorization,
                                        int serviceId,
                                        String mrp,
                                        String price,
                                        int mainCategoryId,
                                        int subCategoryId,
                                        int lastCategoryId) {

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.updateService("Bearer " + authorization, serviceId, mrp, price, mainCategoryId, subCategoryId, lastCategoryId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateServiceStatus(String apiToken, int serviceId, int status) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateServiceStatus("Bearer " + apiToken, serviceId, status);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changePassword(String apiToken, int clientId, String currentPassword, String newPassword) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + apiToken, clientId, currentPassword, newPassword);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrdersWrapper fetchOrderHistory(String apiToken, int userId) {
        OrdersWrapper ordersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderHistory("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    ordersWrapper = null;
                }else{
                    ordersWrapper = gson.fromJson(responseBody, OrdersWrapper.class);
                }
            } else {
                ordersWrapper = null;
            }
        }catch (Exception e){
            ordersWrapper = null;
        }
        return ordersWrapper;
    }

    public CommonResponse changeOrderStatus(String apiToken, int orderId, int status, String reason) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changeOrderStatus("Bearer " + apiToken, orderId, status, reason);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                commonResponse = gson.fromJson(responseBody, CommonResponse.class);
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OtpResponse sendOtp(String mobile) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(mobile);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public CommonResponse resetPassword(String mobile,
                                        String password,
                                        String otp
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> reset = mRepository.resetPassword(otp, mobile, password, password);
            Response<ResponseBody> response = reset.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateFirebaseToken( String authorization,
                                               int userId,
                                               String token
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateFirebaseToken("Bearer " + authorization, userId, token);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse requestPasswordChange( String mobileNumber) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestPassword(mobileNumber, 2);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addDeal(   String authorization,
                                     int clientServcieId,
                                     String discount,
                                     String expiryDate
                                     ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addDeal("Bearer " + authorization, clientServcieId, expiryDate, discount);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public ServicesWrapper fetchServiceList(String apiToken, int clientId) {
        ServicesWrapper servicesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchServicesList("Bearer " + apiToken, clientId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    servicesWrapper = null;
                }else{
                    servicesWrapper = gson.fromJson(responseBody, ServicesWrapper.class);
                }
            } else {
                servicesWrapper = null;
            }
        }catch (Exception e){
            servicesWrapper = null;
        }
        return servicesWrapper;
    }

    public ServicesWrapper fetchDeals(String apiToken, int clientId) {
        ServicesWrapper servicesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDeals("Bearer " + apiToken, clientId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    servicesWrapper = null;
                }else{
                    servicesWrapper = gson.fromJson(responseBody, ServicesWrapper.class);
                }
            } else {
                servicesWrapper = null;
            }
        }catch (Exception e){
            servicesWrapper = null;
        }
        return servicesWrapper;
    }

    public CommonResponse removeDeal(String apiToken, int clientServiceId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeDeal("Bearer " + apiToken, clientServiceId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addCombo  (String authorization,
                                     String comboName,
                                     int mainCategoryId,
                                     ArrayList<String> serviceNames,
                                     ArrayList<String> mrp,
                                     ArrayList<String> price) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addCombo("Bearer " + authorization, comboName, mainCategoryId, serviceNames, mrp, price);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public ComboDataWrapper fetchCombos(String apiToken) {
        ComboDataWrapper comboDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCombos("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    comboDataWrapper = null;
                }else{
                    comboDataWrapper = gson.fromJson(responseBody, ComboDataWrapper.class);
                }
            } else {
                comboDataWrapper = null;
            }
        }catch (Exception e){
            comboDataWrapper = null;
        }
        return comboDataWrapper;
    }

    public CommonResponse editCombo  (String authorization,
                                      String comboName,
                                      int mainCategoryId,
                                      ArrayList<String> serviceNames,
                                      ArrayList<String> mrps,
                                      ArrayList<String> prices,
                                      int comboId,
                                      ArrayList<Integer> serviceIds
                                     ) {

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.editCombo("Bearer " + authorization, comboName, mainCategoryId, serviceNames, mrps, prices, comboId, serviceIds);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public MessageWrapper fetchMessagelist(String apiToken, int page) {
        MessageWrapper messageWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.fetchMessagelist("Bearer " + apiToken, page);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse){
                    messageWrapper = null;
                } else {
                    messageWrapper = gson.fromJson(responseBody, MessageWrapper.class);
                }
            } else {
                messageWrapper = null;
            }
        }catch (Exception e){
            messageWrapper = null;
        }
        return messageWrapper;
    }


}
