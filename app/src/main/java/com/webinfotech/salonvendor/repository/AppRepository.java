package com.webinfotech.salonvendor.repository;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppRepository {

    @GET("signup/otp/send/{mobile}/{user_type}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile,
                               @Path("user_type") int userType);

    @POST("client/registration")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("name") String name,
                                    @Field("mobile") String mobile,
                                    @Field("password") String password,
                                    @Field("otp") String otp,
                                    @Field("clientType") int clientType,
                                    @Field("service_city_id") int serviceCityId,
                                    @Field("latitude") double latitude,
                                    @Field("longitude") double longitude
    );

    @GET("service/city/list")
    Call<ResponseBody> fetchCityList();

    @POST("client/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(  @Field("mobile") String mobile,
                                    @Field("password") String password
    );

    @POST("client/gallery/image/add")
    @Multipart
    Call<ResponseBody> addImages(   @Header("Authorization") String authorization,
                                    @Part("client_id") RequestBody clientId,
                                    @Part MultipartBody.Part image
    );

    @GET("client/profile/{id}")
    Call<ResponseBody> fetchUserProfile(    @Header("Authorization") String authorization,
                                            @Path("id") int id
                                        );

    @GET("client/gallery/image/delete/{client_id}/{image_id}")
    Call<ResponseBody> deleteGalleryImage(  @Header("Authorization") String authorization,
                                            @Path("client_id") int clientId,
                                            @Path("image_id") int image_id
    );

    @POST("client/profile/update")
    @Multipart
    Call<ResponseBody> updateClient(   @Header("Authorization") String authorization,
                                       @Part("client_id") RequestBody clientId,
                                       @Part("name") RequestBody name,
                                       @Part("mobile") RequestBody mobile,
                                       @Part("work_experience") RequestBody workExperience,
                                       @Part("state") RequestBody state,
                                       @Part("city") RequestBody city,
                                       @Part("service_city_id") RequestBody serviceCityId,
                                       @Part("address") RequestBody address,
                                       @Part("pin") RequestBody pin,
                                       @Part("email") RequestBody email,
                                       @Part("gst") RequestBody gst,
                                       @Part("latitude") RequestBody latitude,
                                       @Part("longitude") RequestBody longitude,
                                       @Part("opening_time") RequestBody openingTime,
                                       @Part("closing_time") RequestBody closing_time,
                                       @Part("description") RequestBody description,
                                       @Part MultipartBody.Part image,
                                       @Part MultipartBody.Part addressProofFile,
                                       @Part("address_proof") RequestBody addressProof,
                                       @Part MultipartBody.Part photoProofFile,
                                       @Part("photo_proof") RequestBody photoProof,
                                       @Part MultipartBody.Part businessProofFile,
                                       @Part("business_proof") RequestBody businessProof,
                                       @Part("ac") RequestBody ac,
                                       @Part("parking") RequestBody parking,
                                       @Part("wifi") RequestBody wifi,
                                       @Part("music") RequestBody music);

    @POST("client/schedule/update")
    @FormUrlEncoded
    Call<ResponseBody> updateSchedule(  @Header("Authorization") String authorization,
                                        @Field("client_id") int clientId,
                                        @Field("date") String date,
                                        @Field("status") int status
    );

    @GET("service/list/category")
    Call<ResponseBody> fetchCategories();

    @GET("sub/category/{main_category_id}")
    Call<ResponseBody> fetchSecondCategory(@Path("main_category_id") int mainCategoryId);

    @GET("third/category/{sub_category_id}")
    Call<ResponseBody> fetchThirdCategory(@Path("sub_category_id") int subCategoryId);

    @POST("client/service/add")
    @FormUrlEncoded
    Call<ResponseBody> addService(  @Header("Authorization") String authorization,
                                    @Field("client_id") int clientId,
                                    @Field("main_category[]") ArrayList<Integer> mainCategoryIds,
                                    @Field("sub_category[]")  ArrayList<Integer> subCategoryIds,
                                    @Field("last_category[]") ArrayList<Integer> lastCategoryIds,
                                    @Field("mrp[]") ArrayList<String> mrp,
                                    @Field("price[]") ArrayList<String> price
                                    );

    @PUT("client/service/update/{service_list_id}")
    @FormUrlEncoded
    Call<ResponseBody> updateService(   @Header("Authorization") String authorization,
                                        @Path("service_list_id") int serviceListId,
                                        @Field("mrp") String mrp,
                                        @Field("price") String price,
                                        @Field("main_category_id") int mainCategoryId,
                                        @Field("sub_category_id") int subCategoryId,
                                        @Field("last_category_id") int lastCategoryId
    );

    @GET("client/service/status/update/{service_id}/{status}")
    Call<ResponseBody> updateServiceStatus( @Header("Authorization") String authorization,
                                            @Path("service_id") int serviceId,
                                            @Path("status") int status
    );

    @PUT("client/change/password/{client_id}")
    @FormUrlEncoded
    Call<ResponseBody> changePassword( @Header("Authorization") String authorization,
                                       @Path("client_id") int clientId,
                                       @Field("current_password") String currentPassword,
                                       @Field("new_password") String newPassword
    );

    @GET("client/order/history")
    Call<ResponseBody> fetchOrderHistory(@Header("Authorization") String authorization);

    @POST("client/order/status")
    @FormUrlEncoded
    Call<ResponseBody> changeOrderStatus(@Header("Authorization") String authorization,
                                         @Field("order_id") int orderId,
                                         @Field("status") int status,
                                         @Field("reason") String reason
    );

    @GET("client/forgot/otp/send/{mobile}")
    Call<ResponseBody> sendOtp(@Path("mobile") String mobile);

    @POST("client/forgot/password/change")
    @FormUrlEncoded
    Call<ResponseBody> resetPassword(@Field("otp") String otp,
                                     @Field("mobile") String mobile,
                                     @Field("password") String password,
                                     @Field("confirm_password") String confirmPassword
                                     );

    @GET("client/update/firebase_token/{id}/{token}")
    Call<ResponseBody> updateFirebaseToken( @Header("Authorization") String authorization,
                                            @Path("id") int userId,
                                            @Path("token") String apiToken
    );

    @POST("password/request/send")
    @FormUrlEncoded
    Call<ResponseBody> requestPassword (  @Field("mobile_number") String mobileNumber,
                                          @Field("user_type") int userType );

    @PUT("client/service/deal/add/{client_service_id}")
    @FormUrlEncoded
    Call<ResponseBody> addDeal(        @Header("Authorization") String authorization,
                                       @Path("client_service_id") int clientServiceId,
                                       @Field("expire_date") String expiryDate,
                                       @Field("discount") String discount
    );

    @GET("client/service/list/{client_id}")
    Call<ResponseBody> fetchServicesList(@Header("Authorization") String authorization,
                                         @Path("client_id") int userId
    );

    @GET("client/service/deal/list/{client_id}")
    Call<ResponseBody> fetchDeals(@Header("Authorization") String authorization,
                                  @Path("client_id") int userId
    );

    @GET("client/service/deal/remove/{client_service_id}")
    Call<ResponseBody> removeDeal(@Header("Authorization") String authorization,
                                     @Path("client_service_id") int clientServiceId
    );

    @POST("client/service/combo/add/update")
    @FormUrlEncoded
    Call<ResponseBody> addCombo(  @Header("Authorization") String authorization,
                                  @Field("combo_name") String comboName,
                                  @Field("main_category") int mainCategoryId,
                                  @Field("service_name[]") ArrayList<String> serviceNames,
                                  @Field("mrp[]") ArrayList<String> mrp,
                                  @Field("price[]") ArrayList<String> price
    );

    @GET("client/service/combo/list")
    Call<ResponseBody> fetchCombos(@Header("Authorization") String authorization);

    @POST("client/service/combo/add/update")
    @FormUrlEncoded
    Call<ResponseBody> editCombo(  @Header("Authorization") String authorization,
                                   @Field("combo_name") String comboName,
                                   @Field("main_category") int mainCategoryId,
                                   @Field("service_name[]") ArrayList<String> serviceNames,
                                   @Field("mrp[]") ArrayList<String> mrps,
                                   @Field("price[]") ArrayList<String> prices,
                                   @Field("combo_id") int comboId,
                                   @Field("service_id[]") ArrayList<Integer> serviceIds
                                   );

    @GET("client/message/list")
    Call<ResponseBody> fetchMessagelist(@Header("Authorization") String authorization,
                                        @Query("page") int page
    );

}
