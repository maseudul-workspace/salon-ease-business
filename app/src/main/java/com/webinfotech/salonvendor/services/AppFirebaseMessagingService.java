package com.webinfotech.salonvendor.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.webinfotech.salonvendor.AndroidApplication;
import com.webinfotech.salonvendor.domain.models.NotificationJson;
import com.webinfotech.salonvendor.domain.models.NotificationModel;
import com.webinfotech.salonvendor.presentation.ui.activities.MainActivity;
import com.webinfotech.salonvendor.util.NotificationUtils;

import androidx.annotation.NonNull;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String message = "";
        int userId = 0;
        String title = "";
        Log.e("LogMsg", "Notification: " + remoteMessage.getData());
        try {
            message = remoteMessage.getData().get("message");
        } catch (Exception e) {
            message = "From Salon Ease";
        }

        try {
            title = remoteMessage.getData().get("title");
        } catch (Exception e) {
            title = "From Salon Ease";
        }

        try {
            userId = Integer.parseInt(remoteMessage.getData().get("user_id"));
        } catch (Exception e) {
            userId = 0;
        }
        Intent resultIntent = new Intent(this, MainActivity.class);
        NotificationModel notificationModel = new NotificationModel(title, message);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        if (userId == 0) {
            notificationUtils.displayNotification(notificationModel, resultIntent);
        } else if(androidApplication.getUserInfo(this) != null && androidApplication.getUserInfo(this).id == userId){
            notificationUtils.displayNotification(notificationModel, resultIntent);
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onSendError(@NonNull String s, @NonNull Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
